module main_timestep_mod

! max threads per block
#define TPB 64
! min blocks per multiprocessor
#define BPM 4

#define MAXKMAX 1024
#define MAXZMAX 1024

implicit none
contains

attributes(global) launch_bounds(TPB, BPM) &
                     subroutine regrid_v_binned(bathymetry, depthTop, depthBottom, cellheight, &
                                                data, height, output, &
                                                iMax, jMax, kMax, zMax, tMax, jmaxData, &
                                                rigidLid, levelsStartAtSurface, extensiveVariable)
    ! finds the particle number of the particle in the first slot
    implicit none
    real           , intent(in)    :: bathymetry(:,:)              ! dimension(iMax, jMax)
    real           , intent(in)    :: depthTop(:,:,:)              ! dimension(iMax,jMax,zMax)
    real           , intent(in)    :: depthBottom(:,:,:)           ! dimension(iMax,jMax,zMax)
    real           , intent(in)    :: cellheight(:,:,:)            ! dimension(iMax,jMax,zMax)
    real           , intent(in)    :: data(:,:,:,:)                ! dimension(iMax,jMaxData,kMax,tMax)
    real           , intent(in)    :: height(:,:,:,:)              ! dimension(iMax,jMax,kMax,tMax)
    real           , intent(inout) :: output(:,:,:,:)              ! dimension(iMax,jMaxData,zMax,tMax)
    integer(kind=4), value         :: iMax, jMax, kMax, zMax, tMax, jMaxData
    logical,         value         :: rigidLid
    logical,         value         :: levelsStartAtSurface
    logical,         value         :: extensiveVariable
    
    integer :: ijt     ! combined horizontal and time index
    integer :: ij      ! combined horizontal index
    integer :: i, j, t ! indexes for this kernel
    integer :: k, z    ! layer index in general coordinates and in z coordinates
    integer :: j1, j2
    real    :: zDepth
    real    :: kDepth, kDepth1, kDepth2, kDepth1Factor, kDepth2Factor
    real    :: fractionFilled  ! how much of the z grid cell is already filled with data from the levels
    real    :: matchingHeight ! height of the intersection between the z and k cells

    real :: myHeight(MAXKMAX), levelBottom(MAXKMAX), levelTop(MAXKMAX)
    real :: myDepthTop(MAXZMAX), myDepthBottom(MAXZMAX), myCellheight(MAXZMAX)

    ijt = blockDim%x * (blockIdx%x - 1) + threadIdx%x

    if (ijt .le. iMax*jMaxData*tMax) then
      ! get local indexes in this thread
      t     = (ijt-1)/(iMax*jMaxData)+1
      ij    = MOD(ijt-1,iMax*jMaxData)+1
      j     = (ij-1)/iMax+1
      i     = MOD(ij-1,iMax)+1
      
      ! get indexes of the two grid cells north and south
      if (jMaxData .eq. jMax-1) then      ! v cells between t cells
        j1=j; j2=j+1
      else if (jMaxData .eq. jMax) then   ! v cells north of t cells
        j1=j; j2=min(j+1,jMax)
      else if (jMaxData .eq. jMax+1) then ! t cells between v cells
        j1=max(j-1,1); j2=min(j,jMax)
      else
        if (ijt .eq. 1) write(*,*) "FATAL: j dimension mismatch between grid and data file."
        stop
      end if

      ! find out minimal total depth between the two cells, this is where we will take the layer heights from
      zDepth=min(bathymetry(i,j1), bathymetry(i,j2))
      kDepth1=0.0; kDepth2=0.0
      do k=1,kMax
        kDepth1=kDepth1+height(i,j1,k,t)
        kDepth2=kDepth2+height(i,j2,k,t)
      end do
      if (kDepth1 .lt. kDepth2) then 
        kDepth = kDepth1; kDepth1Factor = 1.0; kDepth2Factor = 0.0
      else if (kDepth1 .gt. kDepth2) then 
        kDepth = kDepth2; kDepth1Factor = 0.0; kDepth2Factor = 1.0
      else  ! exactly identical
        kDepth = kDepth1; kDepth1Factor = 0.5; kDepth2Factor = 0.5
      end if

      ! if rigid lid, scale the layer heights so their sum matches the z depth
      if (rigidLid) then
        kDepth1Factor = kDepth1Factor * zDepth / kDepth
        kDepth2Factor = kDepth2Factor * zDepth / kDepth
      end if

      ! read data into local arrays, so it gets faster
      do z=1,zMax
        myDepthTop(z) = depthTop(i,j1,z)
        myDepthBottom(z) = min(depthBottom(i,j1,z), depthBottom(i,j2,z))
        myCellheight(z) = min(cellheight(i,j1,z), cellheight(i,j2,z))
      end do

      do k=1,kMax
        myHeight(k) = height(i,j1,k,t)*kDepth1Factor + height(i,j2,k,t)*kDepth2Factor
      end do
      if (levelsStartAtSurface) then
        levelTop(1) = 0.0
        levelBottom(1) = myHeight(1)
        do k=2,kMax
          levelTop(k) = levelBottom(k-1)
          levelBottom(k) = levelTop(k) + myHeight(k)
        end do
      else
        levelTop(kMax) = 0.0
        levelBottom(kMax) = myHeight(kMax)
        do k=kMax-1,1,-1
          levelTop(k) = levelBottom(k+1)
          levelBottom(k) = levelTop(k) + myHeight(k)
        end do
      end if
      
      ! double loop over z and k indexes
      do z=1,zMax
        if (myCellheight(z) .gt. 0.0) then
          output(i,j,z,t) = 0.0
          fractionFilled=0.0
          do k=1,kMax
            call syncthreads()
            if (levelBottom(k) .lt. myDepthTop(z)) cycle
            if (levelTop(k) .gt. myDepthBottom(z)) cycle
            matchingHeight = max(0.0, min(levelBottom(k), myDepthBottom(z)) - max(levelTop(k), myDepthTop(z)))
            matchingHeight = matchingHeight/myCellheight(z) ! normalize to cell thickness
            if (extensiveVariable) then
              ! normalize to layer thickness rather than z-cell thickness
              output(i,j,z,t) = output(i,j,z,t) + matchingHeight * data(i,j,k,t) * myCellheight(z)/(levelBottom(k)-levelTop(k))
            else
              output(i,j,z,t) = output(i,j,z,t) + matchingHeight * data(i,j,k,t)
            end if
            fractionFilled=fractionFilled+matchingHeight
          end do
          if (fractionFilled .le. 0.0) then
            output(i,j,z,t) = -1.0e20
          else
            output(i,j,z,t) = output(i,j,z,t) / fractionFilled
          end if
        end if
      end do
    end if
  end subroutine regrid_v_binned

  attributes(global) launch_bounds(TPB, BPM) &
                     subroutine regrid_u_binned(bathymetry, depthTop, depthBottom, cellheight, &
                                                data, height, output, &
                                                iMax, jMax, kMax, zMax, tMax, imaxData, &
                                                rigidLid, levelsStartAtSurface, extensiveVariable)
    ! finds the particle number of the particle in the first slot
    implicit none
    real           , intent(in)    :: bathymetry(:,:)              ! dimension(iMax, jMax)
    real           , intent(in)    :: depthTop(:,:,:)              ! dimension(iMax,jMax,zMax)
    real           , intent(in)    :: depthBottom(:,:,:)           ! dimension(iMax,jMax,zMax)
    real           , intent(in)    :: cellheight(:,:,:)            ! dimension(iMax,jMax,zMax)
    real           , intent(in)    :: data(:,:,:,:)                ! dimension(iMaxData,jMax,kMax,tMax)
    real           , intent(in)    :: height(:,:,:,:)              ! dimension(iMax,jMax,kMax,tMax)
    real           , intent(inout) :: output(:,:,:,:)              ! dimension(iMaxData,jMax,zMax,tMax)
    integer(kind=4), value         :: iMax, jMax, kMax, zMax, tMax, iMaxData
    logical,         value         :: rigidLid
    logical,         value         :: levelsStartAtSurface
    logical,         value         :: extensiveVariable
    
    integer :: ijt     ! combined horizontal and time index
    integer :: ij      ! combined horizontal index
    integer :: i, j, t ! indexes for this kernel
    integer :: k, z    ! layer index in general coordinates and in z coordinates
    integer :: i1, i2
    real    :: zDepth
    real    :: kDepth, kDepth1, kDepth2, kDepth1Factor, kDepth2Factor
    real    :: fractionFilled  ! how much of the z grid cell is already filled with data from the levels
    real    :: matchingHeight ! height of the intersection between the z and k cells

    real :: myHeight(MAXKMAX), levelBottom(MAXKMAX), levelTop(MAXKMAX)
    real :: myDepthTop(MAXZMAX), myDepthBottom(MAXZMAX), myCellheight(MAXZMAX)

    ijt = blockDim%x * (blockIdx%x - 1) + threadIdx%x

    if (ijt .le. iMaxData*jMax*tMax) then
      ! get local indexes in this thread
      t     = (ijt-1)/(iMaxData*jMax)+1
      ij    = MOD(ijt-1,iMaxData*jMax)+1
      j     = (ij-1)/iMaxData+1
      i     = MOD(ij-1,iMaxData)+1
      
      ! get indexes of the two grid cells west and east
      if (iMaxData .eq. iMax-1) then      ! u cells between t cells
        i1=i; i2=i+1
      else if (iMaxData .eq. iMax) then   ! u cells east of t cells
        i1=i; i2=min(i+1,iMax)
      else if (iMaxData .eq. iMax+1) then ! t cells between u cells
        i1=max(i-1,1); i2=min(i,iMax)
      else
        if (ijt .eq. 1) write(*,*) "FATAL: i dimension mismatch between grid and data file."
        stop
      end if

      ! find out minimal total depth between the two cells, this is where we will take the layer heights from
      zDepth=min(bathymetry(i1,j), bathymetry(i2,j))
      kDepth1=0.0; kDepth2=0.0
      do k=1,kMax
        kDepth1=kDepth1+height(i1,j,k,t)
        kDepth2=kDepth2+height(i2,j,k,t)
      end do
      if (kDepth1 .lt. kDepth2) then 
        kDepth = kDepth1; kDepth1Factor = 1.0; kDepth2Factor = 0.0
      else if (kDepth1 .gt. kDepth2) then 
        kDepth = kDepth2; kDepth1Factor = 0.0; kDepth2Factor = 1.0
      else  ! exactly identical
        kDepth = kDepth1; kDepth1Factor = 0.5; kDepth2Factor = 0.5
      end if

      ! if rigid lid, scale the layer heights so their sum matches the z depth
      if (rigidLid) then
        kDepth1Factor = kDepth1Factor * zDepth / kDepth
        kDepth2Factor = kDepth2Factor * zDepth / kDepth
      end if

      ! read data into local arrays, so it gets faster
      do z=1,zMax
        myDepthTop(z) = depthTop(i1,j,z)
        myDepthBottom(z) = min(depthBottom(i1,j,z), depthBottom(i2,j,z))
        myCellheight(z) = min(cellheight(i1,j,z), cellheight(i2,j,z))
      end do

      do k=1,kMax
        myHeight(k) = height(i1,j,k,t)*kDepth1Factor + height(i2,j,k,t)*kDepth2Factor
      end do
      if (levelsStartAtSurface) then
        levelTop(1) = 0.0
        levelBottom(1) = myHeight(1)
        do k=2,kMax
          levelTop(k) = levelBottom(k-1)
          levelBottom(k) = levelTop(k) + myHeight(k)
        end do
      else
        levelTop(kMax) = 0.0
        levelBottom(kMax) = myHeight(kMax)
        do k=kMax-1,1,-1
          levelTop(k) = levelBottom(k+1)
          levelBottom(k) = levelTop(k) + myHeight(k)
        end do
      end if
      
      ! double loop over z and k indexes
      do z=1,zMax
        if (myCellheight(z) .gt. 0.0) then
          output(i,j,z,t) = 0.0
          fractionFilled=0.0
          do k=1,kMax
            call syncthreads()
            if (levelBottom(k) .lt. myDepthTop(z)) cycle
            if (levelTop(k) .gt. myDepthBottom(z)) cycle
            matchingHeight = max(0.0, min(levelBottom(k), myDepthBottom(z)) - max(levelTop(k), myDepthTop(z)))
            matchingHeight = matchingHeight/myCellheight(z) ! normalize to cell thickness
            if (extensiveVariable) then
              ! normalize to layer thickness rather than z-cell thickness
              output(i,j,z,t) = output(i,j,z,t) + matchingHeight * data(i,j,k,t) * myCellheight(z)/(levelBottom(k)-levelTop(k))
            else
              output(i,j,z,t) = output(i,j,z,t) + matchingHeight * data(i,j,k,t)
            end if
            fractionFilled=fractionFilled+matchingHeight
          end do
          if (fractionFilled .le. 0.0) then
            output(i,j,z,t) = -1.0e20
          else
            output(i,j,z,t) = output(i,j,z,t) / fractionFilled
          end if
        end if
      end do
    end if
  end subroutine regrid_u_binned

  attributes(global) launch_bounds(TPB, BPM) &
                     subroutine regrid_t_binned(bathymetry, depthTop, depthBottom, cellheight, &
                                                data, height, output, &
                                                iMax, jMax, kMax, zMax, tMax, &
                                                rigidLid, levelsStartAtSurface, extensiveVariable)
    ! finds the particle number of the particle in the first slot
    implicit none
    real           , intent(in)    :: bathymetry(:,:)              ! dimension(iMax, jMax)
    real           , intent(in)    :: depthTop(:,:,:)              ! dimension(iMax,jMax,zMax)
    real           , intent(in)    :: depthBottom(:,:,:)           ! dimension(iMax,jMax,zMax)
    real           , intent(in)    :: cellheight(:,:,:)            ! dimension(iMax,jMax,zMax)
    real           , intent(in)    :: data(:,:,:,:)                ! dimension(iMax,jMax,kMax,tMax)
    real           , intent(in)    :: height(:,:,:,:)              ! dimension(iMax,jMax,kMax,tMax)
    real           , intent(inout) :: output(:,:,:,:)              ! dimension(iMax,jMax,zMax,tMax)
    integer(kind=4), value         :: iMax, jMax, kMax, zMax, tMax
    logical,         value         :: rigidLid
    logical,         value         :: levelsStartAtSurface
    logical,         value         :: extensiveVariable
    
    integer :: ijt     ! combined horizontal and time index
    integer :: ij      ! combined horizontal index
    integer :: i, j, t ! indexes for this kernel
    integer :: k, z    ! layer index in general coordinates and in z coordinates
    integer :: i1, i2
    real    :: zDepth
    real    :: kDepth, kDepthFactor
    real    :: fractionFilled  ! how much of the z grid cell is already filled with data from the levels
    real    :: matchingHeight ! height of the intersection between the z and k cells

    real :: myHeight(MAXKMAX), levelBottom(MAXKMAX), levelTop(MAXKMAX)
    real :: myDepthTop(MAXZMAX), myDepthBottom(MAXZMAX), myCellheight(MAXZMAX)

    ijt = blockDim%x * (blockIdx%x - 1) + threadIdx%x

    if (ijt .le. iMax*jMax*tMax) then
      ! get local indexes in this thread
      t     = (ijt-1)/(iMax*jMax)+1
      ij    = MOD(ijt-1,iMax*jMax)+1
      j     = (ij-1)/iMax+1
      i     = MOD(ij-1,iMax)+1

      ! find out minimal total depth between the two cells, this is where we will take the layer heights from
      zDepth=bathymetry(i,j)
      kDepth=0.0
      do k=1,kMax
        kDepth=kDepth+height(i,j,k,t)
      end do

      ! if rigid lid, scale the layer heights so their sum matches the z depth
      if (rigidLid) then
        kDepthFactor = zDepth / kDepth
      else
        kDepthFactor = 1.0
      end if

      ! read data into local arrays, so it gets faster
      do z=1,zMax
        myDepthTop(z) = depthTop(i,j,z)
        myDepthBottom(z) = depthBottom(i,j,z)
        myCellheight(z) = cellheight(i,j,z)
      end do

      do k=1,kMax
        myHeight(k) = height(i,j,k,t)*kDepthFactor
      end do
      if (levelsStartAtSurface) then
        levelTop(1) = 0.0
        levelBottom(1) = myHeight(1)
        do k=2,kMax
          levelTop(k) = levelBottom(k-1)
          levelBottom(k) = levelTop(k) + myHeight(k)
        end do
      else
        levelTop(kMax) = 0.0
        levelBottom(kMax) = myHeight(kMax)
        do k=kMax-1,1,-1
          levelTop(k) = levelBottom(k+1)
          levelBottom(k) = levelTop(k) + myHeight(k)
        end do
      end if
      
      ! double loop over z and k indexes
      do z=1,zMax
        if (myCellheight(z) .gt. 0.0) then
          output(i,j,z,t) = 0.0
          fractionFilled=0.0
          do k=1,kMax
            call syncthreads()
            if (levelBottom(k) .lt. myDepthTop(z)) cycle
            if (levelTop(k) .gt. myDepthBottom(z)) cycle
            matchingHeight = max(0.0, min(levelBottom(k), myDepthBottom(z)) - max(levelTop(k), myDepthTop(z)))
            matchingHeight = matchingHeight/myCellheight(z) ! normalize to cell thickness
            if (extensiveVariable) then
              ! normalize to layer thickness rather than z-cell thickness
              output(i,j,z,t) = output(i,j,z,t) + matchingHeight * data(i,j,k,t) * myCellheight(z)/(levelBottom(k)-levelTop(k))
            else
              output(i,j,z,t) = output(i,j,z,t) + matchingHeight * data(i,j,k,t)
            end if
            fractionFilled=fractionFilled+matchingHeight
          end do
          if (fractionFilled .le. 0.0) then
            output(i,j,z,t) = -1.0e20
          else
            output(i,j,z,t) = output(i,j,z,t) / fractionFilled
          end if
        end if
      end do
    end if
  end subroutine regrid_t_binned

subroutine main_timestep
  use variables_gpu2zax_mod
  use cudafor
  use cuda_check_mod
  use netcdf_check_mod
  use netcdf
  implicit none

  integer         :: firstTimestep, lastTimestep
  integer(kind=4) :: istat
  type(dim3)      :: grid, tBlock   

  firstTimestep = (step-1)*timestepsPerStep+1
  lastTimestep  = min(step*timestepsPerStep,tAxisLength)

  if (firstTimestep .eq. lastTimestep) then
    write(*,'(A,I8,A,I8,A)') "   converting time step ",firstTimestep," / ",tAxisLength,"..."; call flush(6)
  else
    write(*,'(A,I8,A,I8,A,I8,A)') "   converting time steps ",firstTimestep," to ",lastTimestep," / ",tAxisLength,"..."; call flush(6)
  end if

  write(*,*) "    reading data..."; call flush(6)
  startData(4) = firstTimestep
  countData(4) = lastTimestep-firstTimestep+1
  call netcdf_check(nf90_get_var(ncidData, varidData, dataBuffer_h, start=startData, count=countData), dataFile, dataVarname)  ! get data at layers
  write(*,*) "    transferring to GPU..."; call flush(6)
  dataBuffer = dataBuffer_h
  istat = cudaDeviceSynchronize()

  write(*,*) "    reading layer heights..."; call flush(6)
  startHeight(4) = firstTimestep
  countHeight(4) = lastTimestep-firstTimestep+1
  call netcdf_check(nf90_get_var(ncidHeight, varidHeight, heightBuffer_h, start=startHeight, count=countHeight), heightFile, heightVarname)  ! get layer thicknesses
  write(*,*) "    transferring to GPU..."; call flush(6)
  heightBuffer = heightBuffer_h
  istat = cudaDeviceSynchronize()

  write(*,*) "    regridding to z levels..."; call flush(6)
  outputBuffer = -1.0e20  ! initialize with missing values
  istat = cudaDeviceSynchronize()
  ! define block size and grid size for calculations on GPU
  tBlock = dim3(TPB,1,1)
  grid = dim3(ceiling(real(xAxisLengthData*yAxisLengthData*(lastTimestep-firstTimestep+1))/tBlock%x),1,1)
  ! call the kernel depending on settings
  if (trim(gridtype) .eq. "T") then
    call regrid_t_binned<<<grid, tblock>>>(bathymetry, depthTop, depthBottom, cellheight, &
                                           dataBuffer, heightBuffer, outputBuffer, &
                                           xAxisLength, yAxisLength, kAxisLength, zAxisLength, int(lastTimestep-firstTimestep+1,kind=4), &
                                           rigidLid, levelsStartAtSurface, extensiveVariable)
  else if (trim(gridtype) .eq. "U") then
    call regrid_u_binned<<<grid, tblock>>>(bathymetry, depthTop, depthBottom, cellheight, &
                                           dataBuffer, heightBuffer, outputBuffer, &
                                           xAxisLength, yAxisLength, kAxisLength, zAxisLength, int(lastTimestep-firstTimestep+1,kind=4), xAxisLengthData, &
                                           rigidLid, levelsStartAtSurface, extensiveVariable)
  else if (trim(gridtype) .eq. "V") then
    call regrid_v_binned<<<grid, tblock>>>(bathymetry, depthTop, depthBottom, cellheight, &
                                           dataBuffer, heightBuffer, outputBuffer, &
                                           xAxisLength, yAxisLength, kAxisLength, zAxisLength, int(lastTimestep-firstTimestep+1,kind=4), yAxisLengthData, &
                                           rigidLid, levelsStartAtSurface, extensiveVariable)
  end if
  istat = cudaDeviceSynchronize()
  outputBuffer_h = outputBuffer
  istat = cudaDeviceSynchronize()

  write(*,*) "    writing output..."; call flush(6)
  startOutput(4) = firstTimestep
  countOutput(4) = lastTimestep-firstTimestep+1
  call netcdf_check(nf90_put_var(ncidOutput, varidOutput, outputBuffer_h, start=startOutput, count=countOutput), outputFile, dataVarname)  ! write to netCDF file
  write(*,*) "    syncing to disk..."; call flush(6)
  call netcdf_check(nf90_sync(ncidOutput))

end subroutine main_timestep
end module main_timestep_mod

