import pandas as pd
from io import StringIO
import re
import os
import glob
from collections import OrderedDict
from netCDF4 import Dataset
import netCDF4 as nc
import datetime
import numpy as np

## FUNCTION DEFINITIONS ##

def read_custom_table(file_path):
    # Read the file content
    with open(file_path, 'r') as file:
        content = file.readlines()

    # Extract variable names from the fourth comment line
    variable_line = [line for line in content if line.strip().startswith('%')][3].strip()
    variables = re.findall(r'\w+(?:\[.*?\])?', variable_line)
    
    # Process variable names
    columns = ['year', 'month', 'day', 'hour', 'minute', 'second']
    for var in variables[1:]:  # Skip 'time'
        name = re.sub(r'\[.*?\]', '', var)  # Remove units in square brackets
        if name not in ['runoff', 'temp']:
            name = 't_' + name
        columns.append(name)

    # Filter out comment lines and empty lines
    data_lines = [line for line in content if not line.strip().startswith('%') and line.strip()]

    # Join the data lines back into a string
    data_content = ''.join(data_lines)

    # Use pandas to read the data
    df = pd.read_csv(StringIO(data_content), delim_whitespace=True, names=columns)

    # Convert the date and time columns to a single datetime column
    df['datetime'] = pd.to_datetime(df[['year', 'month', 'day', 'hour', 'minute', 'second']])
    df = df.drop(['year', 'month', 'day', 'hour', 'minute', 'second'], axis=1)

    return df

def read_river_locations(filename):
    variable_data = {}
    current_variable = None

    with open(filename, 'r') as file:
        for line in file:
            if line and not line.startswith(' '):
                # This is a variable name
                line = line.strip()
                current_variable = line
                variable_data[current_variable] = []
            elif line and line.startswith(' '):
                # This is an index pair
                line = line.strip()
                indices = line.split(';')
                if len(indices) == 2:
                    variable_data[current_variable].append((int(indices[0]), int(indices[1])))

    return variable_data

def filter_dataframes(dataframes, variable_data):
    # Create a new OrderedDict with only the variables from variable_data
    filtered_dataframes = OrderedDict()

    for key in dataframes.keys():
        if key in variable_data:
            filtered_dataframes[key] = dataframes[key]

    return filtered_dataframes

def make_table_for_var(varname, river_locations, dataframes):
    # Create a new empty DataFrame to store the transformed data
    result_df = pd.DataFrame()

    # Iterate through each river and its locations
    for river, locations in river_locations.items():
        if river in dataframes:
            df = dataframes[river]
            # For each location pair, create a new column in the result DataFrame
            for x, y in locations:
                column_name = f"{river}_{x}_{y}"
                if (varname == "runoff"):
                    result_df[column_name] = df[varname]/len(locations)
                else:
                    result_df[column_name] = df[varname]
    return result_df

def extract_numbers(s):
    numbers = re.findall(r'\d+', s)
    return list(map(int, numbers[-2:])) if len(numbers) >= 2 else None

## MAIN PROGRAM STARTS ##
# Get all .dat files in the dat_files directory
dat_files = glob.glob('dat_files/*.dat')

# Create an OrderedDict to store DataFrames with their file names
dataframes = OrderedDict()

# Loop over all matching files
for file_path in dat_files:
    # Get the file name without the path
    file_name = os.path.splitext(os.path.basename(file_path))[0]

    # Read the file into a DataFrame
    df = read_custom_table(file_path)

    # Add the DataFrame to the OrderedDict with the file name as the key
    dataframes[file_name] = df

# Extract column names, excluding 'datetime'
first_key, first_value = list(dataframes.items())[0]
my_variables = [col for col in first_value.columns if col != 'datetime']
#print(my_variables)
time_values = first_value['datetime'].values.astype('datetime64[ns]')

# Read the river locations from the file
river_locations = read_river_locations("river_locations.txt")
dataframes = filter_dataframes(dataframes, river_locations)

# Read xaxis and yaxis
# Open the NetCDF file
nc_file = nc.Dataset('grid_spec.nc', 'r')
# Read the 'lon' and 'lat' variables
xaxis = nc_file.variables['grid_x_T'][:]
yaxis = nc_file.variables['grid_y_T'][:]
# Convert to numpy arrays if they aren't already
xaxis = np.array(xaxis)
yaxis = np.array(yaxis)
# Close the NetCDF file
nc_file.close()

# Create NetCDF file
filename = "rivers.nc"
firsttime=True
with Dataset(filename, "w", format="NETCDF4") as nc_file:

    # Loop over variables
    for my_variable in my_variables:
        my_table = make_table_for_var(my_variable, river_locations, dataframes)
        # Define dimensions
        if firsttime:
            # create netcdf dimensions
            rows, cols = my_table.shape
            nc_file.createDimension("time", rows)
            nc_file.createDimension("river_index", cols)

            # write time axis
            time_data = nc_file.createVariable("time", np.float64, ("time"))
            time_data.units="days since 1970-01-01 00:00:00"
            time_data.calendar="gregorian"
            time_data[:] = time_values

            # extract i and j indexes
            indexes = [extract_numbers(col) for col in my_table.columns]
            # Separate the indexes into vector_i and vector_j
            vector_i = [idx[0] for idx in indexes if idx]
            vector_j = [idx[1] for idx in indexes if idx]
            # get longitude and latitude
            vector_lon = [xaxis[idx-1] for idx in vector_i]
            vector_lat = [yaxis[idx-1] for idx in vector_j]
            # write these variables
            lon_data = nc_file.createVariable("lon", np.float64, ("river_index"))
            lon_data.units = "degrees_east"
            lon_data[:] = vector_lon
            lat_data = nc_file.createVariable("lat", np.float64, ("river_index"))
            lat_data.units = "degrees_north"
            lat_data[:] = vector_lat

            firsttime=False

        # Create variables
        table_data = nc_file.createVariable(my_variable, np.float32, ("time", "river_index"))
        table_data[:] = my_table


