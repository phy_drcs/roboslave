module particles_destroy_mod
! This module destroys particles if there are too many in one grid cell
  implicit none

#include "cuda_settings.inc"

  contains

  attributes(global) launch_bounds(CUDA_TPB_PARTICLES, CUDA_BPM_PARTICLES) &
                     subroutine particles_destroy_find_first_particle(particleCell, particleSlot, firstParticleNum, numParticles, iMax)
    ! finds the particle number of the particle in the first slot
    implicit none
    integer     , intent(in)    :: particleCell(:)                  ! dimension(numParticlesMax)
    integer     , intent(in)    :: particleSlot(:)                  ! dimension(numParticlesMax)  ! only check the particle in slot 1 for destruction
    integer     , intent(inout) :: firstParticleNum(:)              ! dimension(iMax)             ! is set to -1 if the particle is destroyed
    integer     , value         :: numParticles
    integer     , value         :: iMax
    
    integer :: i  ! grid cell index
    integer :: p  ! particle number

    p = blockDim%x * (blockIdx%x - 1) + threadIdx%x
    if (p .le. numParticles) then
      i = particleCell(p)
      if ((i .gt. 0) .and. (i .le. iMax)) then
        ! calculate how many particles we have in comparison to the expected number based on the cell volume
        if (particleSlot(p) .eq. 1) then
          firstParticleNum(i) = p
        end if
      end if
    end if

  end subroutine particles_destroy_find_first_particle

  attributes(global) launch_bounds(CUDA_TPB_PARTICLES, CUDA_BPM_PARTICLES) &
                     subroutine particles_destroy_kernel(particleAtSlot, cellVolume, firstParticleNum, timeAggregatedParticleExcess, & 
                                                         particleCellNew, numParticles, iMax, lMax, particleVolume, timestepsBeforeCreateOrDestroy, acceptableParticleExcess, &
                                                         myOrigGridIndex, indexIAtOrigGrid3d, kMax)
    ! finds the particle number of the particle in the first slot
    implicit none
    logical     , intent(in)    :: particleAtSlot(:,:)              ! dimension(iMax, lMax)
    real        , intent(in)    :: cellVolume(:)                    ! dimension(iMax)  
    integer     , intent(in)    :: firstParticleNum(:)              ! dimension(iMax)             
    real        , intent(inout) :: timeAggregatedParticleExcess(:)  ! dimension(iMax)  
    integer     , intent(inout) :: particleCellNew(:)               ! dimension(numParticlesMax)
    integer     , value         :: numParticles
    integer     , value         :: iMax
    integer     , value         :: lMax
    real        , value         :: particleVolume
    real        , value         :: timestepsBeforeCreateOrDestroy
    real        , value         :: acceptableParticleExcess
    integer     , intent(in)    :: myOrigGridIndex(:,:)
    integer     , intent(in)    :: indexIAtOrigGrid3d(:,:,:)
    integer     , value         :: kMax
    
    integer :: i  ! grid cell index
    integer :: p  ! particle number
    integer :: l  ! slot index
    integer :: np ! number of particles in cell
    real    :: particleExcess
    real    :: fractionNew

    i = blockDim%x * (blockIdx%x - 1) + threadIdx%x
    if (i .le. iMax) then
      ! calculate how large the current mismatch is between the volume of the particles in the cell and the cell volume
      particleExcess = 0.0
      np=0
      do l=1,lMax
        if (particleAtSlot(i,l)) then
          particleExcess = particleExcess + 1.0
          np=np+1
        end if
      end do
      particleExcess = particleExcess - cellVolume(i)/particleVolume
      if (abs(particleExcess) .le. acceptableParticleExcess) then
        particleExcess = 0.0
      else if (particleExcess .lt. 0.0) then
        particleExcess = particleExcess + acceptableParticleExcess
      else
        particleExcess = particleExcess - acceptableParticleExcess
      end if
      ! aggregate the particleExcess over time
      if (timestepsBeforeCreateOrDestroy .gt. 0) then
        fractionNew = 0.5 / timestepsBeforeCreateOrDestroy  ! 0.5 because this is 50% of a time step only, since this is calculated before create and before destroy
        timeAggregatedParticleExcess(i) = timeAggregatedParticleExcess(i)*(1.0-fractionNew) + particleExcess*fractionNew
      else
        timeAggregatedParticleExcess(i) = particleExcess
      end if
      ! check if both the current and the aggregated particle access are above 0.5
      if ((timeAggregatedParticleExcess(i) .gt. 0.5) .and. (particleExcess .gt. 0.5)) then
        ! mark particle for destruction
        if (firstParticleNum(i) .gt. 0) then
          particleCellNew(firstParticleNum(i)) = -1
          !if (mod(firstParticleNum(i),1000) .eq. 0) then
            !write(*,*) "destroy ii=",myOrigGridIndex(i,1)
            !write(*,*) "destroy jj=",myOrigGridIndex(i,2)
            !write(*,*) "destroy kk=",myOrigGridIndex(i,3)
          !  if (myOrigGridIndex(i,3) .eq. kMax) then
          !    write(*,*) "destroy in bottom cell"
          !  else
          !    if (indexIAtOrigGrid3d(myOrigGridIndex(i,1),myOrigGridIndex(i,2),myOrigGridIndex(i,3)+1) .eq. -1) then
          !      write(*,*) "destroy in bottom cell"
          !    else
          !      write(*,*) "destroy elsewhere"
          !    end if
          !  end if
          !end if
          !write(*,*) "delete particle in cell ",i
          !write(*,*) "  cellVolume = ",cellVolume(i)
          !write(*,*) "  particleVolume = ",particleVolume
          !write(*,*) "  ratio = ",cellVolume(i)/particleVolume
          !write(*,*) "  np = ",np
          !write(*,*) "  particleExcess = ",particleExcess
          !write(*,*) "  fractionNew = ",fractionNew
          !write(*,*) "  timeAggregatedParticleExcess = ",timeAggregatedParticleExcess(i)
        end if
      end if
    end if
  end subroutine particles_destroy_kernel

  ! Main function
  subroutine particles_destroy
    use cudafor
    use variables_mod
    use variables_roboadvect_mod
    implicit none

    integer(kind=4) :: istat
    integer :: p

    ! for each grid cell, find the particle number of the particle with index 1
    particleCell     = particleCell_h
    particleCellNew  = particleCell
    particleSlot     = particleSlot_h
    particleAtSlot   = particleAtSlot_h
    firstParticleNum = -1
    tBlock = dim3(CUDA_TPB_PARTICLES,1,1)
    grid = dim3(ceiling(real(numParticlesMax)/tBlock%x),1,1)
    call particles_destroy_find_first_particle<<<grid,tBlock>>>(particleCell, particleSlot, firstParticleNum, numParticles, iMax)
    istat=cudaDeviceSynchronize()

    ! for each grid cell, see if first particle needs to be destroyed
    grid = dim3(ceiling(real(iMax)/tBlock%x),1,1)
    call particles_destroy_kernel<<<grid,tBlock>>>(particleAtSlot, cellVolume, firstParticleNum, timeAggregatedParticleExcess, & 
                                                   particleCellNew, numParticles, iMax, lMax, particleVolume, timestepsBeforeCreateOrDestroy, acceptableParticleExcess, &
                                                   myOrigGridIndex, indexIAtOrigGrid3d, kMax)
    istat=cudaDeviceSynchronize()
    particleCellNew_h = particleCellNew
    istat=cudaDeviceSynchronize()
    numDestroyDuringDestroy=0
    do p=1,numParticles
      if (particleCellNew_h(p) .eq. -1) numDestroyDuringDestroy=numDestroyDuringDestroy+1
    end do

  end subroutine particles_destroy

end module particles_destroy_mod

