module particles_newgridcell_mod
! This module calculates the new grid cells that the particles want to move to.

#include "cuda_settings.inc"

  contains

  subroutine particles_newgridcell_randomjump
    use variables_mod
    use variables_roboadvect_mod
    implicit none

    integer :: i

    ! store old values of particleCellNew temporarily in particleSlotNew_h
    particleSlotNew_h = particleCellNew

    ! for all particles, get a grid cell to which they might be jumping
    call random_number(particleRandomNumber_h)
    particleCellNew_h = 1 + floor(particleRandomNumber_h*iMax)

    ! only keep this new grid cell with the given probability, otherwise keep the old grid cell
    call random_number(particleRandomNumber_h)
    do i=1,numParticles
      if ((particleRandomNumber_h(i) .ge. randomJumpProbability) .or. (particleSlotNew_h(i) .eq. -1)) then  ! -1 means the particle was marked to be destroyed
        particleCellNew_h(i) = particleSlotNew_h(i)
      end if
    end do
    
  end subroutine particles_newgridcell_randomjump

  attributes(global) launch_bounds(CUDA_TPB_PARTICLES, CUDA_BPM_PARTICLES) &
                     subroutine particles_newgridcell_neighbourjump_kernel(neighbourCellIndex, relativeVelocity, particleCell, particleCellNew, particleRandomNumber, &
                                                                           numParticles, iMax, shift, timeStepFraction)
    implicit none
    integer     , intent(in)    :: neighbourCellIndex(:,:)    ! dimension(iMax,6)
    real        , intent(in)    :: relativeVelocity(:,:)      ! dimension(iMax,6)
    integer     , intent(in)    :: particleCell(:)            ! dimension(numParticlesMax)
    integer     , intent(inout) :: particleCellNew(:)         ! dimension(numParticlesMax)
    real(kind=8), intent(in)    :: particleRandomNumber(:)    ! dimension(numParticlesMax)
    integer     , value         :: numParticles
    integer     , value         :: iMax
    integer     , value         :: shift  ! we do not always want to start with the first direction (westward) as this may introduce biases, so we can add "shift" to start with direction (1+shift) mod 6
    real        , value         :: timeStepFraction  ! sub-timestep divided by dt

    integer :: d, dd  ! direction
    integer :: i  ! grid cell index
    integer :: p  ! particle number

    p = blockDim%x * (blockIdx%x - 1) + threadIdx%x
    if (p .le. numParticles) then
      i = particleCell(p)
      if ((i .gt. 0) .and. (i .le. iMax)) then
        do dd=1,6
          d = MOD(dd+shift-1, 6)+1
          if ((d .eq. 1) .or. (d .eq. 3) .or. (d .eq. 5)) then  
            if (0.0-relativeVelocity(i,d)*timeStepFraction .gt. particleRandomNumber(p)) then  ! jump westward / southward / downward
              if (particleCellNew(p) .ne. -1) then
                particleCellNew(p) = neighbourCellIndex(i,d)
              end if
            end if
          end if
          if ((d .eq. 2) .or. (d .eq. 4) .or. (d .eq. 6)) then  
            if (relativeVelocity(i,d)*timeStepFraction .gt. particleRandomNumber(p)) then  ! jump eastward / northward / upward
              if (particleCellNew(p) .ne. -1) then
                particleCellNew(p) = neighbourCellIndex(i,d)
              end if
            end if
          end if
        end do
      end if
    end if

  end subroutine particles_newgridcell_neighbourjump_kernel

  subroutine particles_newgridcell_neighbourjump
    use variables_mod
    use variables_roboadvect_mod
    implicit none

    integer         :: i
    integer(kind=4) :: istat

    particleCell    = particleCell_h
    !particleCellNew was already filled in particles_destroy


    tBlock = dim3(CUDA_TPB_PARTICLES,1,1)
    grid = dim3(ceiling(real(numParticlesMax)/tBlock%x),1,1)

    do i=1,subTimestepsNeighbourjump
      call random_number(particleRandomNumber_h)
      particleRandomNumber = particleRandomNumber_h
      call particles_newgridcell_neighbourjump_kernel<<<grid, tblock>>>(neighbourCellIndex ,relativeVelocity, particleCell, particleCellNew, particleRandomNumber, & 
                                                                        numParticles, iMax, timestep, 1.0/subTimestepsNeighbourjump)                                                                        
      istat = cudaDeviceSynchronize()
      particleCell = particleCellNew
      istat = cudaDeviceSynchronize()
    end do

    particleCell      = particleCell_h
    particleCellNew_h = particleCellNew

  end subroutine particles_newgridcell_neighbourjump

  attributes(global) launch_bounds(CUDA_TPB_PARTICLES, CUDA_BPM_PARTICLES) &
                     subroutine particles_newgridcell_conservative_kernel(neighbourCellIndex, relativeVelocity, particleCell, particleCellNew, particleLocation, &
                                                                          timeStepCompleted, numParticles, iMax, myOrigGridIndex)
    implicit none
    integer     , intent(in)    :: neighbourCellIndex(:,:)    ! dimension(iMax,6)
    real        , intent(in)    :: relativeVelocity(:,:)      ! dimension(iMax,6)
    integer     , intent(in)    :: particleCell(:)            ! dimension(numParticlesMax)
    integer     , intent(inout) :: particleCellNew(:)         ! dimension(numParticlesMax)
    real        , intent(inout) :: particleLocation(:,:)      ! dimension(iMax,6)
    real(kind=8), intent(inout) :: timeStepCompleted(:)       ! dimension(numParticlesMax)
    integer     , value         :: numParticles
    integer     , value         :: iMax
    integer     , intent(in)    :: myOrigGridIndex(:,:)       ! dimension(iMax,3)

    integer :: d, dd  ! direction
    integer :: i  ! grid cell index
    integer :: p  ! particle number
    real    :: u0, alpha  ! velocity at left corner and velocity gradient
    real    :: minTime, newTime    ! time at which particle hits the cell boundary
    real    :: newLocation(3)

    p = blockDim%x * (blockIdx%x - 1) + threadIdx%x
    if (p .le. numParticles) then
      if (particleCellNew(p) .eq. -1) then  !this particle was already destroyed
        timeStepCompleted(p) = 1.0
      else
        i = particleCell(p)
        if ((i .gt. 0) .and. (i .le. iMax)) then
          if (timeStepCompleted(p) .lt. 1.0) then  ! this particle still has to move
            ! STEP 1: Calculate the location of the particle after a full time step.
            !         Use the formula x(t) = -u0/alpha + (x0 + u0/alpha)*exp(alpha*(t-t0))
            !         If alpha is less than 1e-10, set alpha to zero and use x(t) = x0 + u0*(t-t0)
            do d=1,3  ! loop over three directions
              u0    = relativeVelocity(i,2*d-1)
              alpha = relativeVelocity(i,2*d) - u0
              if (abs(alpha) .ge. 1e-10) then
                newLocation(d) = -u0/alpha + (particleLocation(p,d) + u0/alpha)*exp(alpha*(1.0-timeStepCompleted(p)))
              else
                newLocation(d) = particleLocation(p,d) + u0*(1.0-timeStepCompleted(p))
              end if
            end do
            ! STEP 2: Check if particle is still inside the same grid cell.
            !         In this case just update the location and set timeStepCompleted to 1.0
            if ((newLocation(1) .ge. 0.0) .and. (newLocation(2) .ge. 0.0) .and. (newLocation(3) .ge. 0.0) .and. &
                (newLocation(1) .le. 1.0) .and. (newLocation(2) .le. 1.0) .and. (newLocation(3) .le. 1.0)) then
              do d=1,3
                particleLocation(p,d) = newLocation(d)
              end do
              timeStepCompleted(p) = 1.0
            else
              ! This particle leaves the grid cell during the time step.
              ! STEP 3: Find out the time when it hits the cell boundary and the direction into which it leaves
              !         To do so, we invert the formula and get
              !         t = ln( (x(t) + u0/alpha) / (x0 + u0/alpha) )/alpha + t0
              !         In case that alpha is <1e-10, we need to invert the other formula and get
              !         t = (x(t)-x0)/u0 + t0
              minTime = 1.0
              dd = 0
              do d=1,3  ! loop over three directions
                u0    = relativeVelocity(i,2*d-1)
                alpha = relativeVelocity(i,2*d) - u0
                if (newLocation(d) .lt. 0.0) then  ! particle may leave west/south/downward
                  if (abs(alpha) .ge. 1e-10) then
                    newTime=log( (0.0 + u0/alpha) / (particleLocation(p,d) + u0/alpha) ) / alpha + timeStepCompleted(p)
                    if ((newTime .lt. minTime) .and. (newTime .gt. timeStepCompleted(p))) then
                      minTime = newTime
                      dd = 2*d-1
                    end if
                  else
                    newTime=(0.0 - particleLocation(p,d)) / u0 + timeStepCompleted(p)
                    if ((newTime .lt. minTime) .and. (newTime .gt. timeStepCompleted(p))) then
                      minTime = newTime
                      dd = 2*d-1
                    end if
                  end if
                else if (newLocation(d) .gt. 1.0) then  ! particle may leave east/north/upward
                  if (abs(alpha) .ge. 1e-10) then
                    newTime=log( (1.0 + u0/alpha) / (particleLocation(p,d) + u0/alpha) ) / alpha + timeStepCompleted(p)
                    if ((newTime .lt. minTime) .and. (newTime .gt. timeStepCompleted(p))) then
                      minTime = newTime
                      dd = 2*d
                    end if
                  else
                    newTime=(1.0 - particleLocation(p,d)) / u0 + timeStepCompleted(p)
                    if ((newTime .lt. minTime) .and. (newTime .gt. timeStepCompleted(p))) then
                      minTime = newTime
                      dd = 2*d
                    end if
                  end if
                end if
              end do
              ! STEP 4: Calculate the location at the collision time with the cell boundary
              do d=1,3  ! loop over three directions
                u0    = relativeVelocity(i,2*d-1)
                alpha = relativeVelocity(i,2*d) - u0
                if (abs(alpha) .ge. 1e-10) then
                  newLocation(d) = -u0/alpha + (particleLocation(p,d) + u0/alpha)*exp(alpha*(minTime-timeStepCompleted(p)))
                else
                  newLocation(d) = particleLocation(p,d) + u0*(minTime-timeStepCompleted(p))
                end if
                newLocation(d) = max(0.0,min(1.0,newLocation(d)))
              end do

              ! if (p .eq. 2195415) then
              !   write(*,*) "pd",p," direction ",dd
              !   write(*,*) "pd",p," rv1 ",relativeVelocity(i,1)
              !   write(*,*) "pd",p," rv2 ",relativeVelocity(i,2)
              !   write(*,*) "pd",p," rv3 ",relativeVelocity(i,3)
              !   write(*,*) "pd",p," rv4 ",relativeVelocity(i,4)
              !   write(*,*) "pd",p," rv5 ",relativeVelocity(i,5)
              !   write(*,*) "pd",p," rv6 ",relativeVelocity(i,6)
              !   write(*,*) "pd",p," nci1 ",neighbourCellIndex(i,1)
              !   write(*,*) "pd",p," nci2 ",neighbourCellIndex(i,2)
              !   write(*,*) "pd",p," nci3 ",neighbourCellIndex(i,3)
              !   write(*,*) "pd",p," nci4 ",neighbourCellIndex(i,4)
              !   write(*,*) "pd",p," nci5 ",neighbourCellIndex(i,5)
              !   write(*,*) "pd",p," nci6 ",neighbourCellIndex(i,6)
              !   write(*,*) "pd",p," ii ",myOrigGridIndex(i,1)
              !   write(*,*) "pd",p," jj ",myOrigGridIndex(i,2)
              !   write(*,*) "pd",p," kk ",myOrigGridIndex(i,3)
              !   write(*,*) "pd",p," oldX ",particleLocation(p,1)
              !   write(*,*) "pd",p," oldY ",particleLocation(p,2)
              !   write(*,*) "pd",p," oldZ ",particleLocation(p,3)
              !   write(*,*) "pd",p," newX ",newLocation(1)
              !   write(*,*) "pd",p," newY ",newLocation(2)
              !   write(*,*) "pd",p," newZ ",newLocation(3)
              ! end if

              particleLocation(p,1) = newLocation(1)
              particleLocation(p,2) = newLocation(2)
              particleLocation(p,3) = newLocation(3)
              timeStepCompleted(p)  = minTime
              
              ! STEP 5: Move to the new cell and correspondingly update the relative location inside the cell
              if (dd .gt. 0) then
                particleCellNew(p) = neighbourCellIndex(i,dd)
                if (dd .eq. 1) particleLocation(p,1) = 1.0  ! westward
                if (dd .eq. 2) particleLocation(p,1) = 0.0  ! eastward
                if (dd .eq. 3) particleLocation(p,2) = 1.0  ! southward
                if (dd .eq. 4) particleLocation(p,2) = 0.0  ! northward
                if (dd .eq. 5) particleLocation(p,3) = 1.0  ! downward
                if (dd .eq. 6) particleLocation(p,3) = 0.0  ! upward
              else
                ! no new cell, maybe for numerical reasons
                particleCellNew(p) = particleCell(p)
              end if
            end if
          end if
        end if
      end if
    end if

  end subroutine particles_newgridcell_conservative_kernel

  subroutine particles_newgridcell_conservative
    use variables_mod
    use variables_roboadvect_mod
    implicit none

    integer         :: i
    integer(kind=4) :: istat

    particleCell    = particleCell_h
    !particleCellNew was already filled in particles_destroy

    tBlock = dim3(CUDA_TPB_PARTICLES,1,1)
    grid = dim3(ceiling(real(numParticlesMax)/tBlock%x),1,1)

    ! use particleRandomNumber as array that stores the relative fraction of the time step that the particle already did
    particleRandomNumber_h = 1.0
    particleRandomNumber_h(1:numParticles) = 0.0  ! these need to be moved
    particleRandomNumber = particleRandomNumber_h

    i=1
    do while ((MINVAL(particleRandomNumber) .lt. 1.0) .and. (i .le. maxIterationsNewGridcell))
      write(*,*) "iteration=",i,", minTime=",MINVAL(particleRandomNumber)
      call particles_newgridcell_conservative_kernel<<<grid, tblock>>>(neighbourCellIndex ,relativeVelocity, particleCell, particleCellNew, particleLocation, &
                                                                       particleRandomNumber, numParticles, iMax, myOrigGridIndex)                                                                        
      istat = cudaDeviceSynchronize()
      particleCell = particleCellNew
      istat = cudaDeviceSynchronize()
      i=i+1
    end do

    particleCell       = particleCell_h
    particleCellNew_h  = particleCellNew
    particleLocation_h = particleLocation
    istat = cudaDeviceSynchronize()

  end subroutine particles_newgridcell_conservative

  ! Main function
  subroutine particles_newgridcell
    use cudafor
    use variables_mod
    use variables_roboadvect_mod
    implicit none

    integer :: p

    if (trim(lagrangeScheme) .eq. "randomjump") then
      call particles_newgridcell_randomjump
    else if (trim(lagrangeScheme) .eq. "neighbourjump") then
      call particles_newgridcell_neighbourjump
    else if (trim(lagrangeScheme) .eq. "conservative") then
      call particles_newgridcell_conservative
    end if

    numDestroyDuringNewGridcell=0
    do p=1,numParticles
      if (particleCellNew_h(p) .eq. -1) then
        numDestroyDuringNewGridcell=numDestroyDuringNewGridcell+1
      end if
    end do
    numDestroyDuringNewGridcell = numDestroyDuringNewGridcell-numDestroyDuringDestroy

  end subroutine particles_newgridcell

end module particles_newgridcell_mod

