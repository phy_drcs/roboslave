module init_output_mod
! This module creates all output files and defines the NetCDF variables
! For some of them, init_bgc_output needs to be called

  contains

  subroutine define_netcdf_dimension(axis, ncid, fileName, recordDimension)
    use types_mod
    use netcdf
    use netcdf_check_mod
    implicit none
    type(gridAxisType), intent(inout) :: axis
    integer(kind=4),    intent(in )   :: ncid
    character(len=255), intent(in)    :: fileName
    logical, optional,  intent(in)    :: recordDimension
    integer(kind=4)                   :: edgesDimId, edgesVarId
    logical                           :: recDim
    
    recDim = .false.
    if (present(recordDimension)) recDim=recordDimension

    if (recDim) then
      call netcdf_check(nf90_def_dim(ncid, axis%name, int(0,kind=4), axis%ncDimId), fileName)                                            ! define axis as record dimension (flexible length)
    else  
      call netcdf_check(nf90_def_dim(ncid, axis%name, int(SIZE(axis%values),kind=4), axis%ncDimId), fileName)                            ! define axis with pre-defined length
    end if
    call netcdf_check(nf90_def_var(ncid, axis%name, NF90_DOUBLE, (/ axis%ncDimId /), axis%ncVarId), fileName)                ! define variable
    if (allocated(axis%edges)) then
      call netcdf_check(nf90_def_dim(ncid, trim(axis%name)//"_edges", int(SIZE(axis%edges),kind=4), edgesDimId), fileName)               ! define edges axis
      call netcdf_check(nf90_def_var(ncid, trim(axis%name)//"_edges", NF90_DOUBLE, (/ edgesDimId /), edgesVarId), fileName)  ! define edges variable
      call netcdf_check(nf90_put_att(ncid, axis%ncVarId, "edges", trim(axis%name)//"_edges"), fileName)                      ! set edges attribute
    end if
    if (axis%long_name .ne. "") then
      call netcdf_check(nf90_put_att(ncid, axis%ncVarId, "long_name", trim(axis%long_name)), fileName)                       ! set long_name attribute
    end if
    if (axis%cartesian_axis .ne. "") then
      call netcdf_check(nf90_put_att(ncid, axis%ncVarId, "cartesian_axis", trim(axis%cartesian_axis)), fileName)             ! set cartesian_axis attribute
      call netcdf_check(nf90_put_att(ncid, axis%ncVarId, "axis",           trim(axis%cartesian_axis)), fileName)             ! set cartesian_axis attribute
    end if
    if (axis%units .ne. "") then
      call netcdf_check(nf90_put_att(ncid, axis%ncVarId, "units", trim(axis%units)), fileName)                               ! set units attribute
    end if
    if (axis%positive .ne. "") then
      call netcdf_check(nf90_put_att(ncid, axis%ncVarId, "positive", trim(axis%positive)), fileName)                         ! set positive attribute
    end if
    call netcdf_check(nf90_enddef(ncid), fileName) ! leave define mode
    call netcdf_check(nf90_put_var(ncid, axis%ncVarId, axis%values), fileName)
    if (allocated(axis%edges)) then
      call netcdf_check(nf90_put_var(ncid, edgesVarId, axis%edges), fileName)
    end if
    call netcdf_check(nf90_redef(ncid), fileName) ! enter define mode again
    
  end subroutine define_netcdf_dimension

  subroutine init_output_roboslave
    use variables_mod
    use variables_roboadvect_mod
    use netcdf
    use netcdf_check_mod
    implicit none

    integer :: i
    integer(kind=4) :: bufferAxisId
    integer(kind=4) :: sendAxisId, destroyAxisId, jumpAxisId, createAxisId, receiveAxisId
    integer(kind=4) :: gridcellAxisId, slotAxisId

    ! allocate buffers
    allocate( sendFromCellBuf(advectionFileBufferLength) )
    allocate( SendFromSlotBuf(advectionFileBufferLength) )
    allocate( SendToDomainBuf(advectionFileBufferLength) )
    allocate( destroyFromCellBuf(advectionFileBufferLength) )
    allocate( destroyFromSlotBuf(advectionFileBufferLength) )                      
    allocate( jumpFromCellBuf(advectionFileBufferLength) )
    allocate( jumpFromSlotBuf(advectionFileBufferLength) )
    allocate( jumpToCellBuf(advectionFileBufferLength) )
    allocate( jumpToSlotBuf(advectionFileBufferLength) )  
    allocate( createToCellBuf(advectionFileBufferLength) )
    allocate( createToSlotBuf(advectionFileBufferLength) )                                
    allocate( receiveFromDomainBuf(advectionFileBufferLength) )
    allocate( receiveToCellBuf(advectionFileBufferLength) )
    allocate( receiveToSlotBuf(advectionFileBufferLength) )

    ! define buffer dimensions
    call netcdf_check(nf90_def_dim(outputFileNcid, trim("buffer"), int(advectionFileBufferLength,kind=4), bufferAxisId), outputFile)  
    call netcdf_check(nf90_def_dim(outputFileNcid, trim("sendaxis"),    int(0,kind=4), sendAxisId),    outputFile)  ! these are all unlimited dimensions, more than one is allowed in NetCDF4
    call netcdf_check(nf90_def_dim(outputFileNcid, trim("destroyaxis"), int(0,kind=4), destroyAxisId), outputFile)  
    call netcdf_check(nf90_def_dim(outputFileNcid, trim("jumpaxis"),    int(0,kind=4), jumpAxisId),    outputFile)  
    call netcdf_check(nf90_def_dim(outputFileNcid, trim("createaxis"),  int(0,kind=4), createAxisId),  outputFile)  
    call netcdf_check(nf90_def_dim(outputFileNcid, trim("receiveaxis"), int(0,kind=4), receiveAxisId), outputFile)  

    ! define gridcell and slot dimensions (for initial_occupancy)
    call netcdf_check(nf90_def_dim(outputFileNcid, trim("gridcell"), int(iMax,kind=4), gridcellAxisId), outputFile) 
    call netcdf_check(nf90_def_dim(outputFileNcid, trim("slot"),     int(lMax,kind=4), slotAxisId),     outputFile) 

    ! define original grid dimensions to write the i index of the cells
    do i=1,SIZE(origGridAxes)
      call define_netcdf_dimension(origGridAxes(i), outputFileNcid, outputFile)
    end do

    ! define all variables along time dimension
    call netcdf_check(nf90_def_var(outputFileNcid, trim("num_send"),    NF90_INT, (/ timeAxis%ncDimId /), numSendVarId),    outputFile)  ! number of particles jumping out to another compute domain
    call netcdf_check(nf90_def_var(outputFileNcid, trim("num_destroy"), NF90_INT, (/ timeAxis%ncDimId /), numDestroyVarId), outputFile)  ! number of particles disappearing
    call netcdf_check(nf90_def_var(outputFileNcid, trim("num_jump"),    NF90_INT, (/ timeAxis%ncDimId /), numJumpVarId),    outputFile)  ! number of particles jumping within the compute domain
    call netcdf_check(nf90_def_var(outputFileNcid, trim("num_create"),  NF90_INT, (/ timeAxis%ncDimId /), numCreateVarId),  outputFile)  ! number of particles appearing
    call netcdf_check(nf90_def_var(outputFileNcid, trim("num_receive"), NF90_INT, (/ timeAxis%ncDimId /), numReceiveVarId), outputFile)  ! number of particles jumping in from another compute domain

    ! define all variables along number dimension
    call netcdf_check(nf90_def_var(outputFileNcid, trim("send_from_cell"), NF90_INT, (/ bufferAxisId, sendAxisId /), sendFromCellVarId), outputFile)  
    call netcdf_check(nf90_def_var(outputFileNcid, trim("send_from_slot"), NF90_BYTE, (/ bufferAxisId, sendAxisId /), sendFromSlotVarId), outputFile) 
    call netcdf_check(nf90_def_var(outputFileNcid, trim("send_to_domain"), NF90_INT, (/ bufferAxisId, sendAxisId /), sendToDomainVarId), outputFile) 

    call netcdf_check(nf90_def_var(outputFileNcid, trim("destroy_from_cell"), NF90_INT, (/ bufferAxisId, destroyAxisId /), destroyFromCellVarId), outputFile) 
    call netcdf_check(nf90_def_var(outputFileNcid, trim("destroy_from_slot"), NF90_BYTE, (/ bufferAxisId, destroyAxisId /), destroyFromSlotVarId), outputFile) 
    
    call netcdf_check(nf90_def_var(outputFileNcid, trim("jump_from_cell"), NF90_INT, (/ bufferAxisId, jumpAxisId /), jumpFromCellVarId),  outputFile) 
    call netcdf_check(nf90_def_var(outputFileNcid, trim("jump_from_slot"), NF90_BYTE, (/ bufferAxisId, jumpAxisId /), jumpFromSlotVarId),  outputFile) 
    call netcdf_check(nf90_def_var(outputFileNcid, trim("jump_to_cell"),   NF90_INT, (/ bufferAxisId, jumpAxisId /), jumpToCellVarId),    outputFile) 
    call netcdf_check(nf90_def_var(outputFileNcid, trim("jump_to_slot"),   NF90_BYTE, (/ bufferAxisId, jumpAxisId /), jumpToSlotVarId),    outputFile) 
    
    call netcdf_check(nf90_def_var(outputFileNcid, trim("create_to_cell"), NF90_INT, (/ bufferAxisId, createAxisId /), createToCellVarId), outputFile) 
    call netcdf_check(nf90_def_var(outputFileNcid, trim("create_to_slot"), NF90_BYTE, (/ bufferAxisId, createAxisId /), createToSlotVarId), outputFile) 

    call netcdf_check(nf90_def_var(outputFileNcid, trim("receive_from_domain"), NF90_INT, (/ bufferAxisId, receiveAxisId /), receiveFromDomainVarId), outputFile) 
    call netcdf_check(nf90_def_var(outputFileNcid, trim("receive_to_cell"),     NF90_INT, (/ bufferAxisId, receiveAxisId /), receiveToCellVarId),     outputFile)  
    call netcdf_check(nf90_def_var(outputFileNcid, trim("receive_to_slot"),     NF90_BYTE, (/ bufferAxisId, receiveAxisId /), receiveToSlotVarId),     outputFile) 

     ! define initial_occupancy variable along gridcell and slot dimensions
    call netcdf_check(nf90_def_var(outputFileNcid, trim("initial_occupancy"), NF90_INT, (/ gridcellAxisId, slotAxisId /), initialOccupancyVarId), outputFile)

    ! define index_i variable along origGrid dimensions
    call netcdf_check(nf90_def_var(outputFileNcid, trim("index_i"), NF90_INT, (/ origGridAxes(1)%ncDimId, origGridAxes(2)%ncDimId, origGridAxes(3)%ncDimId /), indexIVarId), outputFile)

    if (debugOutputRoboadvect) then
      allocate(particlesDestroyedHere  (size(origGridAxes(1)%values), size(origGridAxes(2)%values), size(origGridAxes(3)%values)))
      allocate(particlesDestroyedHere_h(size(origGridAxes(1)%values), size(origGridAxes(2)%values), size(origGridAxes(3)%values)))
      particlesDestroyedHere_h = 0; particlesDestroyedHere = 0
      allocate(particlesJumpedFromHere  (size(origGridAxes(1)%values), size(origGridAxes(2)%values), size(origGridAxes(3)%values)))
      allocate(particlesJumpedFromHere_h(size(origGridAxes(1)%values), size(origGridAxes(2)%values), size(origGridAxes(3)%values)))
      particlesJumpedFromHere_h = 0; particlesJumpedFromHere = 0
      allocate(particlesJumpedToHere  (size(origGridAxes(1)%values), size(origGridAxes(2)%values), size(origGridAxes(3)%values)))
      allocate(particlesJumpedToHere_h(size(origGridAxes(1)%values), size(origGridAxes(2)%values), size(origGridAxes(3)%values)))
      particlesJumpedToHere_h = 0; particlesJumpedToHere = 0
      allocate(particlesCreatedHere  (size(origGridAxes(1)%values), size(origGridAxes(2)%values), size(origGridAxes(3)%values)))
      allocate(particlesCreatedHere_h(size(origGridAxes(1)%values), size(origGridAxes(2)%values), size(origGridAxes(3)%values)))
      particlesCreatedHere_h = 0; particlesCreatedHere = 0

      call netcdf_check(nf90_def_var(outputFileNcid, trim("destroyed_here"), NF90_INT, (/ origGridAxes(1)%ncDimId, origGridAxes(2)%ncDimId, origGridAxes(3)%ncDimId /), destroyedHereVarId), outputFile)
      call netcdf_check(nf90_def_var(outputFileNcid, trim("jumped_from_here"), NF90_INT, (/ origGridAxes(1)%ncDimId, origGridAxes(2)%ncDimId, origGridAxes(3)%ncDimId /), jumpedFromHereVarId), outputFile)
      call netcdf_check(nf90_def_var(outputFileNcid, trim("jumped_to_here"), NF90_INT, (/ origGridAxes(1)%ncDimId, origGridAxes(2)%ncDimId, origGridAxes(3)%ncDimId /), jumpedToHereVarId), outputFile)
      call netcdf_check(nf90_def_var(outputFileNcid, trim("created_here"), NF90_INT, (/ origGridAxes(1)%ncDimId, origGridAxes(2)%ncDimId, origGridAxes(3)%ncDimId /), createdHereVarId), outputFile)
    end if

    ! leave define mode of netCDF file
    call netcdf_check(nf90_enddef(outputFileNcid), outputFile)
    
    ! write index_i 
    call netcdf_check(nf90_put_var(outputFileNcid, indexIVarId, indexIAtOrigGrid3d_h), outputFile, "index_i")

    ! initialize output buffers
    sendFromCellBuf=0; SendFromSlotBuf=0; SendToDomainBuf=0
    destroyFromCellBuf=0; destroyFromSlotBuf=0
    jumpFromCellBuf=0; jumpFromSlotBuf=0; jumpToCellBuf=0; jumpToSlotBuf=0
    createToCellBuf=0; createToSlotBuf=0
    receiveFromDomainBuf=0; receiveToCellBuf=0; receiveToSlotBuf=0
    sendI=1;    sendJ=1
    destroyI=1; destroyJ=1
    jumpI=1;    jumpJ=1
    createI=1;  createJ=1
    receiveI=1; receiveJ=1
   
  end subroutine init_output_roboslave

  ! Main function
  subroutine init_output
    use variables_mod
    use netcdf
    use netcdf_check_mod
    implicit none
    integer            :: stat

    ! delete output file if exists
    open(unit=1234, iostat=stat, file=outputFile, status='old')
    if (stat == 0) close(1234, status='delete')

    ! create NetCDF file
    call netcdf_check(nf90_create(outputFile, NF90_NETCDF4, outputFileNcid), outputFile) 
    
    ! define time dimension
    call define_netcdf_dimension(timeAxis, outputFileNcid, outputFile, .false.)  ! time is not the record dimension

    ! now do initializations depending on outputStyle
    if (outputStyle .eq. "roboslave") then
      call init_output_roboslave
    else if (outputStyle .eq. "trajectories") then
      ! call init_output_trajectories
    else
      write(*,*) "FATAL: outputStyle in input/input.nml has a value " // outputStyle // " which is not recognized."
      write(*,*) "Supported values: roboslave trajectories"
      stop
    end if

    call netcdf_check(nf90_sync(outputFileNcid),   outputFile)

  end subroutine init_output

end module init_output_mod

