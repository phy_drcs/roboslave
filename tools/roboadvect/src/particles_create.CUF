module particles_create_mod
! This module creates particles if there are too few in one grid cell
  implicit none

#include "cuda_settings.inc"

  contains

  attributes(global) launch_bounds(CUDA_TPB_PARTICLES, CUDA_BPM_PARTICLES) &
                     subroutine particles_create_kernel(particleAtSlot, cellVolume, timeAggregatedParticleExcess, createParticleHere, & 
                                                        iMax, lMax, particleVolume, timestepsBeforeCreateOrDestroy, acceptableParticleExcess)
    ! finds the particle number of the particle in the first slot
    implicit none
    logical     , intent(in)    :: particleAtSlot(:,:)              ! dimension(iMax, lMax)
    real        , intent(in)    :: cellVolume(:)                    ! dimension(iMax)  
    real        , intent(inout) :: timeAggregatedParticleExcess(:)  ! dimension(iMax)  
    integer     , intent(inout) :: createParticleHere(:)            ! dimension(iMax)
    integer     , value         :: iMax
    integer     , value         :: lMax
    real        , value         :: particleVolume
    real        , value         :: timestepsBeforeCreateOrDestroy
    real        , value         :: acceptableParticleExcess

    
    integer :: i  ! grid cell index
    integer :: p  ! particle number
    integer :: l  ! slot index
    integer :: numParticlesHere
    real    :: particleExcess
    real    :: fractionNew

    i = blockDim%x * (blockIdx%x - 1) + threadIdx%x
    if (i .le. iMax) then
      ! calculate how large the current mismatch is between the volume of the particles in the cell and the cell volume
      numParticlesHere = 0
      do l=1,lMax
        if (particleAtSlot(i,l)) then
          numParticlesHere = numParticlesHere + 1
        end if
      end do
      particleExcess = numParticlesHere - cellVolume(i)/particleVolume
      if (abs(particleExcess) .le. acceptableParticleExcess) then
        particleExcess = 0.0
      else if (particleExcess .lt. 0.0) then
        particleExcess = particleExcess + acceptableParticleExcess
      else
        particleExcess = particleExcess - acceptableParticleExcess
      end if
      ! aggregate the particleExcess over time
      if (timestepsBeforeCreateOrDestroy .gt. 0) then
        fractionNew = 0.5 / timestepsBeforeCreateOrDestroy  ! 0.5 because this is 50% of a time step only, since this is calculated before create and before destroy
        timeAggregatedParticleExcess(i) = timeAggregatedParticleExcess(i)*(1.0-fractionNew) + particleExcess*fractionNew
      else
        timeAggregatedParticleExcess(i) = particleExcess
      end if
      ! check if both the current and the aggregated particle access are below -0.5
      if ((timeAggregatedParticleExcess(i) .lt. -0.5) .and. (particleExcess .lt. -0.5)) then
        if (numParticlesHere .lt. lMax) then 
          createParticleHere(i) = 1
        end if
      end if
    end if
  end subroutine particles_create_kernel

  ! Main function
  subroutine particles_create
    use cudafor
    use variables_mod
    use variables_roboadvect_mod
    implicit none

    integer         :: i, p
    integer(kind=4) :: istat
    real, allocatable :: randomArray(:)

    ! for each grid cell, see if a particle needs to be created
    createParticleHere = 0
    particleAtSlot = particleAtSlot_h
    istat=cudaDeviceSynchronize()
    grid = dim3(ceiling(real(iMax)/tBlock%x),1,1)
    call particles_create_kernel<<<grid,tBlock>>>(particleAtSlot, cellVolume, timeAggregatedParticleExcess, createParticleHere, & 
                                                   iMax, lMax, particleVolume, timestepsBeforeCreateOrDestroy, acceptableParticleExcess)
    istat=cudaDeviceSynchronize()
    ! transfer this information to host memory
    createParticleHere_h = createParticleHere

    ! how many particles do we have to create?
    numCreate = SUM(createParticleHere_h)
    if (numCreate .gt. 0) then
      if (numParticles+numCreate .le. numParticlesMax) then
        if (useParticleLocation) then
          allocate(randomArray(numCreate*3))
          call random_number(randomArray)
        end if
        p = 1 
        do i=1,iMax
          if (createParticleHere_h(i) .gt. 0) then
            createToCell_h(p) = i
            ! find a free slot, it is certain that one exists, since otherwise the particles_create_kernel would not have placed a particle here
            createToSlot_h(p) = 1
            do while (particleAtSlot_h(i,createToSlot_h(p)))
              createToSlot_h(p) = createToSlot_h(p) + 1
              if (createToSlot_h(p) .gt. lMax) then
                write(*,*) "FATAL: creating a particle in a grid cell that is already full, internal program error"
                stop
              end if
            end do
            ! update occupancy array
            particleAtSlot_h(i,createToSlot_h(p)) = .true.
            particleCell_h(numParticles+p) = i
            particleSlot_h(numParticles+p) = createToSlot_h(p)
            ! define location of the new particle
            if (useParticleLocation) then
              particleLocation_h(numParticles+p,1) = randomArray(p)
              particleLocation_h(numParticles+p,2) = randomArray(p+numCreate)
              particleLocation_h(numParticles+p,3) = randomArray(p+2*numCreate)
            end if
            ! increase particle index
            p=p+1
          end if
        end do
        particleCell = particleCell_h
        particleSlot = particleSlot_h
        if (useParticleLocation) then
          particleLocation = particleLocation_h
        end if
        numParticles = numParticles + numCreate
      else
        write(*,*) "FATAL: Too many particles need to be created."
        write(*,*) "       Please increase numParticlesMax or set it to 0 which means iMax*lMax=",iMax*lMax
        stop
      end if
    end if

  end subroutine particles_create

end module particles_create_mod

