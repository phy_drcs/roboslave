DESTDIR=/sw/data/netcdf/OS_15.4/x86_64-suse-linux/4.9.1/nvfortran
module load pgi/nvhpc/22.7

wget https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-1.13/hdf5-1.13.2/src/hdf5-1.13.2.tar.gz
tar xvaf hdf5-1.13.2.tar.gz
cd hdf5-1.13.2
mkdir build
cd build
../configure CC=nvc FC=nvfortran \
    CFLAGS="-fPIC -O1" \
    FCFLAGS="-fPIC -O1" \
    --enable-shared \
    --enable-static \
    --enable-fortran \
    --enable-hl \
    --with-zlib \
    --with-szlib=$DESTDIR \
    --prefix=$DESTDIR
make -j
make install
cd ../..
