DESTDIR=/sw/data/netcdf/OS_15.4/x86_64-suse-linux/4.9.1/nvfortran
module load pgi/nvhpc/22.7

wget https://support.hdfgroup.org/ftp/lib-external/szip/2.1.1/src/szip-2.1.1.tar.gz
tar xvaf szip-2.1.1.tar.gz
cd szip-2.1.1
mkdir build
cd build
../configure CC=nvc \
    --enable-shared \
    --enable-static \
    --prefix=$DESTDIR
make -j
make install
cd ../..
