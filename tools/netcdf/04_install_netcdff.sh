DESTDIR=/sw/data/netcdf/OS_15.4/x86_64-suse-linux/4.9.1/nvfortran
module load pgi/nvhpc/22.7

rm -r netcdf-fortran-4.6.0
wget https://github.com/Unidata/netcdf-fortran/archive/refs/tags/v4.6.0.tar.gz && \
    mv v4.6.0.tar.gz netcdf-fortran-4.6.0.tar.gz
tar xvaf netcdf-fortran-4.6.0.tar.gz
cd netcdf-fortran-4.6.0
mkdir build 
cd build 
export LIB_NETCDF=$DESTDIR/lib64
export LD_LIBRARY_PATH=$LIB_NETCDF:$LD_LIBRARY_PATH
../configure CC=nvc FC=nvfortran \
    CFLAGS="-fPIC -O1 -I$DESTDIR/include" \
    FCFLAGS="-fPIC -O1" \
    LDFLAGS="-L$DESTDIR/lib64 -lnetcdf -lhdf5_hl -lhdf5" \
    --enable-shared \
    --enable-static \
    --prefix=$DESTDIR
make
make install
