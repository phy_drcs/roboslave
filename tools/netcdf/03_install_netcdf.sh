DESTDIR=/sw/data/netcdf/OS_15.4/x86_64-suse-linux/4.9.1/nvfortran
module load pgi/nvhpc/22.7

ln -s $DESTDIR/lib $DESTDIR/lib64
wget https://github.com/Unidata/netcdf-c/archive/refs/tags/v4.9.0.tar.gz && \
    mv v4.9.0.tar.gz netcdf-c-4.9.0.tar.gz
tar xvaf netcdf-c-4.9.0.tar.gz
cd netcdf-c-4.9.0
mkdir build
cd build
export LIB_NETCDF=$DESTDIR/lib64
export LD_LIBRARY_PATH=$LIB_NETCDF:$LD_LIBRARY_PATH
../configure CC=nvc \
    CFLAGS="-fPIC -O1" \
    CPPFLAGS="-I$DESTDIR/include" \
    LDFLAGS="-L$DESTDIR/lib64" \
    --enable-shared \
    --enable-static \
    --prefix=$DESTDIR
make -j
make install
cd ../..
