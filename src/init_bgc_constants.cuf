module init_bgc_constants_mod
contains

  subroutine init_bgc_constants
  ! This subroutine sets the values of the biogeochemical constants used in the model
    use variables_mod, only : constants_h
    implicit none
    integer :: n

    !!!!!!!!!!!!!!!!!!!!!!!
    ! use standard values !
    !!!!!!!!!!!!!!!!!!!!!!!
    real, parameter :: critical_stress = 0.125    ! critical shear stress for sediment erosion [N/m2]
    real, parameter :: cya0            = 1.0E-6   ! seed concentration for diazotroph cyanobacteria [mol/kg]
    real, parameter :: din_min_lpp     = 1.125E-6 ! DIN half saturation constant for large-cell phytoplankton growth [mol/kg]
    real, parameter :: din_min_spp     = 1.5E-7   ! DIN half saturation constant for small-cell phytoplankton growth [mol/kg]
    real, parameter :: dip_min_cya     = 1.1E-8   ! DIP half saturation constant for diazotroph cyanobacteria growth [mol/kg]
    real, parameter :: din_min_lip     = 1.5E-6   ! DIN half saturation constant for limnic phytoplankton growth [mol/kg]
    real, parameter :: epsilon         = 4.5E-17  ! no division by 0
    real, parameter :: food_min_zoo    = 4.108E-6 ! Ivlev phytoplankton concentration for zooplankton grazing [mol/kg]
    real, parameter :: gamma0          = 0.027    ! light attentuation parameter (opacity of clear water) [1/m], DO NOT CHANGE NAME gamma0 SINCE THIS NAME WILL BE USED IN THE TEMPLATE
    real, parameter :: gamma1          = 58.0     ! light attentuation parameter (opacity of POM containing chlorophyll) [m**2/mol]
    real, parameter :: gamma2          = 53.2     ! light attentuation parameter (opacity of POM detritus) [m**2/mol]
    real, parameter :: gamma3          = 12.6     ! light attentuation parameter (opacity of DON) [m**2/mol]
    real, parameter :: h2s_min_po4_liber = 1.0E-6   ! minimum h2s concentration for liberation of iron phosphate from the sediment [mol/kg]
    real, parameter :: ips_threshold   = 0.1      ! threshold for increased PO4 burial [mol/m**2]
    real, parameter :: ips_cl          = 0.02025  ! iron phosphate in sediment closure parameter [mol/m2]
    real, parameter :: k_h2s_no3       = 800000.0 ! reaction constant h2s oxidation with no3 [kg/mol/day]
    real, parameter :: k_h2s_o2        = 800000.0 ! reaction constant h2s oxidation with o2 [kg/mol/day]
    real, parameter :: k_sul_no3       = 20000.0  ! reaction constant sul oxidation with no3 [kg/mol/day]
    real, parameter :: k_sul_o2        = 20000.0  ! reaction constant sul oxidation with o2 [kg/mol/day]
    real, parameter :: light_opt_cya   = 50.0     ! optimal light for diazotroph cyanobacteria growth [W/m**2]
    real, parameter :: light_opt_lpp   = 55.0     ! optimal light for large-cell phytoplankton growth [W/m**2]
    real, parameter :: light_opt_spp   = 65.0     ! optimal light for small-cell phytoplankton growth [W/m**2]
    real, parameter :: light_opt_lip   = 20.0     ! optimal light for limnic phytoplankton growth [W/m**2]
    real, parameter :: lip0            = 1.0E-8   ! seed concentration for limnic phytoplankton [mol/kg]
    real, parameter :: lpp0            = 5.0E-9   ! seed concentration for large-cell phytoplankton [mol/kg]
    real, parameter :: no3_min_sed_denit = 1.423E-7 ! nitrate half-saturation concentration for denitrification in the water column [mol/kg]
    real, parameter :: no3_min_det_denit = 1.0E-9   ! minimum no3 concentration for recycling of detritus using nitrate (denitrification)
    real, parameter :: o2_min_det_resp = 1.0E-6   ! oxygen half-saturation constant for detritus recycling [mol/kg]
    real, parameter :: o2_min_nit      = 3.75E-6  ! oxygen half-saturation constant for nitrification [mol/kg]
    real, parameter :: o2_min_po4_retent = 0.0000375 ! oxygen half-saturation concentration for retension of phosphate during sediment denitrification [mol/kg]
    real, parameter :: o2_min_sed_resp = 0.000064952 ! oxygen half-saturation constant for recycling of sediment detritus using oxygen [mol/kg]
    real, parameter :: patm_co2        = 38.0     ! atmospheric partial pressure of CO2 [Pa]
    real, parameter :: q10_det_rec     = 0.15     ! q10 rule factor for recycling [1/K]
    real, parameter :: q10_doc_rec     = 0.069    ! q10 rule factor for DOC recycling [1/K]
    real, parameter :: q10_h2s         = 0.0693   ! q10 rule factor for oxidation of h2s and sul [1/K]
    real, parameter :: q10_nit         = 0.11     ! q10 rule factor for nitrification [1/K]
    real, parameter :: q10_sed_rec     = 0.244    ! q10 rule factor for detritus recycling in the sediment [1/K]
    real, parameter :: r_biores        = 0.015    ! bio-resuspension rate [1/day]
    real, parameter :: r_cya_assim     = 1.0      ! maximum rate for nutrient uptake of diazotroph cyanobacteria [1/day]
    real, parameter :: r_cya_resp      = 0.01     ! respiration rate of cyanobacteria to ammonium [1/day]
    real, parameter :: r_det_rec       = 0.003    ! recycling rate (detritus to ammonium) at 0�C [1/day]
    real, parameter :: r_ips_burial    = 0.007    ! final burial rate for PO4 [1/day]
    real, parameter :: r_ips_ero       = 6.0      ! erosion rate for iron PO4 [1/day]
    real, parameter :: r_ips_liber     = 0.1      ! PO4 liberation rate under anoxic conditions [1/day]
    real, parameter :: r_lpp_assim     = 1.125    ! maximum rate for nutrient uptake of large-cell phytoplankton [1/day]
    real, parameter :: r_lpp_resp      = 0.075    ! respiration rate of large phytoplankton to ammonium [1/day]
    real, parameter :: r_lip_assim     = 3.0      ! maximum rate for nutrient uptake of limnic phytoplankton [1/day]
    real, parameter :: r_lip_resp      = 0.075    ! respiration rate of limnic phytoplankton to ammonium [1/day]
    real, parameter :: r_nh4_nitrif    = 0.05     ! nitrification rate at 0�C [1/day]
    real, parameter :: r_pp_mort       = 0.03     ! mortality rate of phytoplankton [1/day]
    real, parameter :: r_cya_mort_diff = 40.0     ! enhanced cya mortality due to strong turbulence
    real, parameter :: r_cya_mort_thresh = 0.015    ! diffusivity threshold for enhanced cyano mortality
    real, parameter :: r_sed_ero       = 6.0      ! maximum sediment detritus erosion rate [1/day]
    real, parameter :: r_sed_rec       = 0.003    ! maximum recycling rate for sedimentary detritus [1/d]
    real, parameter :: r_sed_poc_rec   = 0.000025 ! maximum recycling rate for sedimentary POC [1/d]
    real, parameter :: r_spp_assim     = 0.4      ! maximum rate for nutrient uptake of small-cell phytoplankton [1/day]
    real, parameter :: r_spp_resp      = 0.0175   ! respiration rate of small phytoplankton to ammonium [1/day]
    real, parameter :: r_zoo_graz      = 0.5      ! maximum zooplankton grazing rate [1/day]
    real, parameter :: r_zoo_mort      = 0.03     ! mortality rate of zooplankton [1/day]
    real, parameter :: r_zoo_resp      = 0.01     ! respiration rate of zooplankton [1/day]
    real, parameter :: rfr_c           = 6.625    ! redfield ratio C/N
    real, parameter :: rfr_h           = 16.4375  ! redfield ratio H/N
    real, parameter :: rfr_o           = 6.875    ! redfield ratio O/N
    real, parameter :: rfr_p           = 0.0625   ! redfield ratio P/N
    real, parameter :: rfr_cp          = 106.0    ! redfield ratio C/P
    real, parameter :: sali_max_cya    = 8.0      ! upper salinity limit - diazotroph cyanobacteria [psu]
    real, parameter :: sali_min_cya    = 4.0      ! lower salinity limit - diazotroph cyanobacteria [psu]
    real, parameter :: nit_max_cya     = 3.0E-7   ! limits cyano growth in DIN reach environment
    real, parameter :: nit_switch_cya  = 6700000.0 ! strengs of DIN control for cyano growth
    real, parameter :: sali_max_lip    = 2.0      ! lower salinity limit - limnic phytoplankton [psu]
    real, parameter :: sed_max         = 0.75     ! maximum sediment detritus concentration that feels erosion [mol/m**2]
    real, parameter :: sed_burial      = 2.0      ! maximum sediment load before burial
    real, parameter :: spp0            = 1.0E-9   ! seed concentration for small-cell phytoplankton [mol/kg]
    real, parameter :: temp_min_cya    = 12.0     ! lower temperature limit - diazotroph cyanobacteria [�C]
    real, parameter :: temp_switch_cya = 4.0      ! strengs of temperature control for cyano growth
    real, parameter :: temp_min_spp    = 10.0     ! lower temperature limit - small-cell phytoplankton [�C]
    real, parameter :: temp_opt_zoo    = 18.0     ! optimal temperature for zooplankton grazing [�C]
    real, parameter :: w_co2_stf       = 0.5      ! piston velocity for co2 surface flux [m/d]
    real, parameter :: w_cya           = 1.0      ! vertical speed of diazotroph cyanobacteria [m/day]
    real, parameter :: w_det           = -4.5     ! vertical speed of detritus [m/day]
    real, parameter :: w_ipw           = -1.0     ! vertical speed of suspended iron PO4 [m/day]
    real, parameter :: w_det_sedi      = -2.25    ! sedimentation velocity (negative for downward) [m/day]
    real, parameter :: w_ipw_sedi      = -0.5     ! sedimentation velocity for iron PO4 [m/day]
    real, parameter :: w_lpp           = -0.5     ! vertical speed of large-cell phytoplankton [m/day]
    real, parameter :: w_n2_stf        = 5.0      ! piston velocity for n2 surface flux [m/d]
    real, parameter :: w_o2_stf        = 5.0      ! piston velocity for oxygen surface flux [m/d]
    real, parameter :: zoo0            = 4.5E-9   ! seed concentration for zooplankton [mol/kg]
    real, parameter :: zoo_cl          = 9.0E-8   ! zooplankton closure parameter [mol/kg]
    real, parameter :: don_fraction    = 0.0      ! fraction of DON in respiration products
    real, parameter :: r_poc_rec       = 0.002    ! recycling rate (poc to dic) at 0�C [1/day]
    real, parameter :: r_pocp_rec      = 0.002    ! recycling rate (pocp to dic and po4) at 0�C [1/day]
    real, parameter :: r_pocn_rec      = 0.002    ! recycling rate (pocn to dic and nh4) at 0�C [1/day]
    real, parameter :: w_poc           = -0.05    ! vertical speed of poc [m/day]
    real, parameter :: w_poc_sedi      = -0.05    ! sedimentation velocity (negative for downward) [m/day]
    real, parameter :: w_pocp          = -1.0     ! vertical speed of pocp [m/day]
    real, parameter :: w_pocp_sedi     = -0.5     ! sedimentation velocity (negative for downward) [m/day]
    real, parameter :: w_pocn          = -1.0     ! vertical speed of pocn [m/day]
    real, parameter :: w_pocn_sedi     = -0.5     ! sedimentation velocity (negative for downward) [m/day]
    real, parameter :: fac_doc_assim_lpp = 0.3      ! factor modifying DOC assimilation rate of large phytoplankton LPP
    real, parameter :: fac_doc_assim_cya = 1.0      ! factor modifying DOC assimilation rate of cyanobacteria
    real, parameter :: fac_doc_assim_spp = 1.0      ! factor modifying DOC assimilation rate of small phytoplankton SPP
    real, parameter :: fac_doc_assim_lip = 1.0      ! factor modifying DOC assimilation rate of limnic phytoplankton LIP
    real, parameter :: fac_dop_assim   = 0.6      ! factor modifying assimilation rate for POCP production
    real, parameter :: fac_don_assim   = 1.0      ! factor modifying assimilation rate for POCN production
    real, parameter :: fac_enh_rec     = 10.0     ! enhance recyclig of DON,POCN/DOP,POCP in case of limiting DIN/DIP
    real, parameter :: ret_po4_1       = 0.1      ! PO4 retension in oxic sediments
    real, parameter :: ret_po4_2       = 0.5      ! additional PO4 retension in oxic sediments of the Bothnian Sea
    real, parameter :: ret_po4_3       = 0.15     ! additional PO4 retension in oxic sediments of the Bothnian Sea
    real, parameter :: frac_denit_scal = 0.6      ! scaling frac_denit_sed
    real, parameter :: reduced_rec     = 0.7      ! decrease recycling in sed under anoxia by reduce_rec
    real, parameter :: martin_fac_poc  = 0.02     ! [1/d], depth dependence of POC sinking speed
    real, parameter :: r_doc2poc       = 0.05     ! POC formation rate
    real, parameter :: r_don2pocn      = 0.005    ! POCN formation rate
    real, parameter :: r_dop2pocp      = 0.0001   ! POCP formation rate
    real, parameter :: r_doc_rec       = 0.002    ! recycling rate (doc to dic) at 0�C [1/day]
    real, parameter :: r_don_rec       = 0.001    ! recycling rate (don to dic and NH4) at 0�C [1/day]
    real, parameter :: r_dop_rec       = 0.002    ! recycling rate (dop to dic and PO4) at 0�C [1/day]
    real, parameter :: fac_ips_burial  = 0.5      ! reduced burial of t_ips, mimicing resolving iron-P complexes in deeper sediment and subsequent upward PO4 flux
    real, parameter :: r_cdom_decay    = 0.004    ! decay rate of cdom
    real, parameter :: r_cdom_light    = 50.0     ! PAR intensity controling CDOM decay
    real, parameter :: alk_btf_0       = 0.005    ! artifical alkalinity bottom flux constant mol/m**2/day
    real, parameter :: alk_btf_0_BBfac = 0.1      ! fraction of BS BFT Alk flux in BB
    real, parameter :: alk_btf_DBBfac  = 0.0      ! Factor for alkalinity BTF below depth D2 in Bothnian Bay
    real, parameter :: alk_btf_Dfac    = 0.075    ! Factor for alkalinity BTF below depth D2
    real, parameter :: alk_btf_D1      = 8.0      ! upper depth for alkalinity dissolution from sea bed
    real, parameter :: alk_btf_D2      = 45.0     ! below depth D2 alkalinity dissolution from sea bed is reduced by alk_btf_Dfac

    !!!!!!!!!!!!!!!!!!!!!!!!!!
    ! save them in the array !
    !!!!!!!!!!!!!!!!!!!!!!!!!!
    n=1
    constants_h(:,n) = critical_stress ; n=n+1
    constants_h(:,n) = cya0            ; n=n+1
    constants_h(:,n) = din_min_lpp     ; n=n+1
    constants_h(:,n) = din_min_spp     ; n=n+1
    constants_h(:,n) = dip_min_cya     ; n=n+1
    constants_h(:,n) = din_min_lip     ; n=n+1
    constants_h(:,n) = epsilon         ; n=n+1
    constants_h(:,n) = food_min_zoo    ; n=n+1
    constants_h(:,n) = gamma0          ; n=n+1
    constants_h(:,n) = gamma1          ; n=n+1
    constants_h(:,n) = gamma2          ; n=n+1
    constants_h(:,n) = gamma3          ; n=n+1
    constants_h(:,n) = h2s_min_po4_liber ; n=n+1
    constants_h(:,n) = ips_threshold   ; n=n+1
    constants_h(:,n) = ips_cl          ; n=n+1
    constants_h(:,n) = k_h2s_no3       ; n=n+1
    constants_h(:,n) = k_h2s_o2        ; n=n+1
    constants_h(:,n) = k_sul_no3       ; n=n+1
    constants_h(:,n) = k_sul_o2        ; n=n+1
    constants_h(:,n) = light_opt_cya   ; n=n+1
    constants_h(:,n) = light_opt_lpp   ; n=n+1
    constants_h(:,n) = light_opt_spp   ; n=n+1
    constants_h(:,n) = light_opt_lip   ; n=n+1
    constants_h(:,n) = lip0            ; n=n+1
    constants_h(:,n) = lpp0            ; n=n+1
    constants_h(:,n) = no3_min_sed_denit ; n=n+1
    constants_h(:,n) = no3_min_det_denit ; n=n+1
    constants_h(:,n) = o2_min_det_resp ; n=n+1
    constants_h(:,n) = o2_min_nit      ; n=n+1
    constants_h(:,n) = o2_min_po4_retent ; n=n+1
    constants_h(:,n) = o2_min_sed_resp ; n=n+1
    constants_h(:,n) = patm_co2        ; n=n+1
    constants_h(:,n) = q10_det_rec     ; n=n+1
    constants_h(:,n) = q10_doc_rec     ; n=n+1
    constants_h(:,n) = q10_h2s         ; n=n+1
    constants_h(:,n) = q10_nit         ; n=n+1
    constants_h(:,n) = q10_sed_rec     ; n=n+1
    constants_h(:,n) = r_biores        ; n=n+1
    constants_h(:,n) = r_cya_assim     ; n=n+1
    constants_h(:,n) = r_cya_resp      ; n=n+1
    constants_h(:,n) = r_det_rec       ; n=n+1
    constants_h(:,n) = r_ips_burial    ; n=n+1
    constants_h(:,n) = r_ips_ero       ; n=n+1
    constants_h(:,n) = r_ips_liber     ; n=n+1
    constants_h(:,n) = r_lpp_assim     ; n=n+1
    constants_h(:,n) = r_lpp_resp      ; n=n+1
    constants_h(:,n) = r_lip_assim     ; n=n+1
    constants_h(:,n) = r_lip_resp      ; n=n+1
    constants_h(:,n) = r_nh4_nitrif    ; n=n+1
    constants_h(:,n) = r_pp_mort       ; n=n+1
    constants_h(:,n) = r_cya_mort_diff ; n=n+1
    constants_h(:,n) = r_cya_mort_thresh ; n=n+1
    constants_h(:,n) = r_sed_ero       ; n=n+1
    constants_h(:,n) = r_sed_rec       ; n=n+1
    constants_h(:,n) = r_sed_poc_rec   ; n=n+1
    constants_h(:,n) = r_spp_assim     ; n=n+1
    constants_h(:,n) = r_spp_resp      ; n=n+1
    constants_h(:,n) = r_zoo_graz      ; n=n+1
    constants_h(:,n) = r_zoo_mort      ; n=n+1
    constants_h(:,n) = r_zoo_resp      ; n=n+1
    constants_h(:,n) = rfr_c           ; n=n+1
    constants_h(:,n) = rfr_h           ; n=n+1
    constants_h(:,n) = rfr_o           ; n=n+1
    constants_h(:,n) = rfr_p           ; n=n+1
    constants_h(:,n) = rfr_cp          ; n=n+1
    constants_h(:,n) = sali_max_cya    ; n=n+1
    constants_h(:,n) = sali_min_cya    ; n=n+1
    constants_h(:,n) = nit_max_cya     ; n=n+1
    constants_h(:,n) = nit_switch_cya  ; n=n+1
    constants_h(:,n) = sali_max_lip    ; n=n+1
    constants_h(:,n) = sed_max         ; n=n+1
    constants_h(:,n) = sed_burial      ; n=n+1
    constants_h(:,n) = spp0            ; n=n+1
    constants_h(:,n) = temp_min_cya    ; n=n+1
    constants_h(:,n) = temp_switch_cya ; n=n+1
    constants_h(:,n) = temp_min_spp    ; n=n+1
    constants_h(:,n) = temp_opt_zoo    ; n=n+1
    constants_h(:,n) = w_co2_stf       ; n=n+1
    constants_h(:,n) = w_cya           ; n=n+1
    constants_h(:,n) = w_det           ; n=n+1
    constants_h(:,n) = w_ipw           ; n=n+1
    constants_h(:,n) = w_det_sedi      ; n=n+1
    constants_h(:,n) = w_ipw_sedi      ; n=n+1
    constants_h(:,n) = w_lpp           ; n=n+1
    constants_h(:,n) = w_n2_stf        ; n=n+1
    constants_h(:,n) = w_o2_stf        ; n=n+1
    constants_h(:,n) = zoo0            ; n=n+1
    constants_h(:,n) = zoo_cl          ; n=n+1
    constants_h(:,n) = don_fraction    ; n=n+1
    constants_h(:,n) = r_poc_rec       ; n=n+1
    constants_h(:,n) = r_pocp_rec      ; n=n+1
    constants_h(:,n) = r_pocn_rec      ; n=n+1
    constants_h(:,n) = w_poc           ; n=n+1
    constants_h(:,n) = w_poc_sedi      ; n=n+1
    constants_h(:,n) = w_pocp          ; n=n+1
    constants_h(:,n) = w_pocp_sedi     ; n=n+1
    constants_h(:,n) = w_pocn          ; n=n+1
    constants_h(:,n) = w_pocn_sedi     ; n=n+1
    constants_h(:,n) = fac_doc_assim_lpp ; n=n+1
    constants_h(:,n) = fac_doc_assim_cya ; n=n+1
    constants_h(:,n) = fac_doc_assim_spp ; n=n+1
    constants_h(:,n) = fac_doc_assim_lip ; n=n+1
    constants_h(:,n) = fac_dop_assim   ; n=n+1
    constants_h(:,n) = fac_don_assim   ; n=n+1
    constants_h(:,n) = fac_enh_rec     ; n=n+1
    constants_h(:,n) = ret_po4_1       ; n=n+1
    constants_h(:,n) = ret_po4_2       ; n=n+1
    constants_h(:,n) = ret_po4_3       ; n=n+1
    constants_h(:,n) = frac_denit_scal ; n=n+1
    constants_h(:,n) = reduced_rec     ; n=n+1
    constants_h(:,n) = martin_fac_poc  ; n=n+1
    constants_h(:,n) = r_doc2poc       ; n=n+1
    constants_h(:,n) = r_don2pocn      ; n=n+1
    constants_h(:,n) = r_dop2pocp      ; n=n+1
    constants_h(:,n) = r_doc_rec       ; n=n+1
    constants_h(:,n) = r_don_rec       ; n=n+1
    constants_h(:,n) = r_dop_rec       ; n=n+1
    constants_h(:,n) = fac_ips_burial  ; n=n+1
    constants_h(:,n) = r_cdom_decay    ; n=n+1
    constants_h(:,n) = r_cdom_light    ; n=n+1
    constants_h(:,n) = alk_btf_0       ; n=n+1
    constants_h(:,n) = alk_btf_0_BBfac ; n=n+1
    constants_h(:,n) = alk_btf_DBBfac  ; n=n+1
    constants_h(:,n) = alk_btf_Dfac    ; n=n+1
    constants_h(:,n) = alk_btf_D1      ; n=n+1
    constants_h(:,n) = alk_btf_D2      ; n=n+1

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! vary them if corresponding files exist !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  end subroutine init_bgc_constants
end module init_bgc_constants_mod
