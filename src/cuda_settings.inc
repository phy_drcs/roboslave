!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! CUDA parameters for alignment of kernel threads on the device !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! CUDA_TPB:
! You need to specify how many threads can run in one block.
! This depends on the number of variables which are in the kernels.
! If there are many variables you need smaller TPB values.
! CUDA_BPM:
! How many blocks per multiprocessor need to run at least at the same time.
! If TPB is small, you need larger BPM to get efficient memory access.
! Set TPB as large as possible but not larger than your GPU allows, e.g. 512.
! Set BPM to such a value that TPB*BPM are at least 512.
! Powers of 2 make sense.
! Individual settings are possible for 
! - the BGC calculations (SUR, WAT, SED and SURSED).
! - the light calculations (LIGHT)
! - the Lagrangian advection calculations (ADVECTION)
! - the transpose kernels (TRANSPOSE_ORIG2VB, TRANSPOSE_VB2EULER, TRANSPOSE_EULER2LAGRANGE)
#define CUDA_TPB_WAT 64
#define CUDA_BPM_WAT 4
#define CUDA_TPB_SUR 64
#define CUDA_BPM_SUR 4
#define CUDA_TPB_SED 64
#define CUDA_BPM_SED 4
#define CUDA_TPB_SURSED 64
#define CUDA_BPM_SURSED 4

#define CUDA_TPB_SEDI 64
#define CUDA_BPM_SEDI 4

#define CUDA_TPB_LIGHT 64
#define CUDA_BPM_LIGHT 4

#define CUDA_TPB_VERTDIFF 64
#define CUDA_BPM_VERTDIFF 4

#define CUDA_TPB_ADVECTION 64
#define CUDA_BPM_ADVECTION 4

#define CUDA_TPB_RIVER 64
#define CUDA_BPM_RIVER 4

#define CUDA_TPB_TRANSPOSE_ORIG2VB 64
#define CUDA_BPM_TRANSPOSE_ORIG2VB 4

#define CUDA_TPB_TRANSPOSE_VB2EULER 64
#define CUDA_BPM_TRANSPOSE_VB2EULER 4

#define CUDA_TPB_TRANSPOSE_EULER2LAGRANGE 64
#define CUDA_BPM_TRANSPOSE_EULER2LAGRANGE 4

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! CUDA parameters for data storage !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! CUDA_VERTBLOCK_WIDTH:
! Data are stored in a series of "vertical blocks" (2-d matrices with horizontal and vertical dimension)
! Since not all horizontal points have the same number of vertical layers, the blocks have a different
! vertical extent.
! Increasing CUDA_VERTBLOCK_WIDTH gives a better alignment of the data in memory at the expense of some memory 
! consumption, since the vertical extent of each block is determined by the maximum number of levels.

#define CUDA_VERTBLOCK_WIDTH 1024
