module transpose_kernel_obc_river
! transpose kernels for OBC and river data

#include "cuda_settings.inc"

contains

  attributes(global) launch_bounds(CUDA_TPB_TRANSPOSE_ORIG2VB, CUDA_BPM_TRANSPOSE_ORIG2VB) &
                     subroutine transpose_obc_x2jk_single(inArray, myArray, n, xLength, kMax, mMax, nVarMax, mnPadding)
  ! This subroutine will copy the variables stored in one vertical block to the
  ! appropriate memory locations for the Eulerian gridpoints
    implicit none
    ! the following four indexes all have dimension(kMax):
    real(kind=4), intent(in)     :: inArray(:)           ! dimension(xLength) values as read from file
    real,         intent(inout)  :: myArray(:,:,:)       ! dimension(mnMax, xLength, kMax) output array
    integer, value               :: n                    ! variable index
    integer, value               :: xLength              ! original grid dimension 1
    integer, value               :: kMax                 ! destination grid dimension 3
    integer, value               :: mMax                 ! number of ensemble members
    integer, value               :: nVarMax              ! number of state variables
    integer, value               :: mnPadding            ! space holder for alignment
    

    integer :: m, j, k     ! indexes for ensemble member, destination grid dimension 2, and destination grid dimension 3
    integer :: mj          ! ensemble member and destination grid dimension 2 combined index
    integer :: mjk         ! ensemble member and destination grid dimension 2 and 3 combined index
    integer :: mn          ! destination grid dimension 1
    
    mjk   = blockDim%x * (blockIdx%x - 1) + threadIdx%x
    k     = (mjk-1)/(mMax*xLength)+1
    mj    = MOD(mjk-1,mMax*xLength)+1
    j     = (mj-1)/mMax+1
    m     = MOD(mj-1,mMax)+1
    
    if (k .le. kMax) then
      mn = m + (n-1)*mMax
      myArray(mn, j, k) = inArray(j)
    end if
  end subroutine transpose_obc_x2jk_single

  attributes(global) launch_bounds(CUDA_TPB_TRANSPOSE_ORIG2VB, CUDA_BPM_TRANSPOSE_ORIG2VB) &
                     subroutine transpose_obc_x2jk_runoff_single(inArray, myArray, n, xLength, kMax, mMax, nVarMax, mnPadding, &
                                                                 eulerWATpar, eulerSURpar, eulerSEDpar, eulerSURSEDpar, &
                                                                 whichArray, arrayIndex)
  ! This subroutine will copy the variables stored in one vertical block to the
  ! appropriate memory locations for the Eulerian gridpoints
    implicit none
    ! the following four indexes all have dimension(kMax):
    real(kind=4), intent(in)     :: inArray(:)           ! dimension(xLength) values as read from file
    real,         intent(inout)  :: myArray(:,:,:)       ! dimension(mnMax, xLength, kMax) output array
    integer, value               :: n                    ! variable index, in this case it is zero
    integer, value               :: xLength              ! original grid dimension 1
    integer, value               :: kMax                 ! destination grid dimension 3
    integer, value               :: mMax                 ! number of ensemble members
    integer, value               :: nVarMax              ! number of state variables
    integer, value               :: mnPadding            ! space holder for alignment
    real,         intent(in)     :: eulerWATpar(:,:)     ! dimension(mMax*iMaxWAT,nParMax) forcing parameters for vertLoc=WAT cells, we need cell height and water column height
    real,         intent(in)     :: eulerSURpar(:,:)     ! dimension(mMax*iMaxSUR,nParMax) forcing parameters for vertLoc=WAT cells, we need cell height and water column height
    real,         intent(in)     :: eulerSEDpar(:,:)     ! dimension(mMax*iMaxSED,nParMax) forcing parameters for vertLoc=WAT cells, we need cell height and water column height
    real,         intent(in)     :: eulerSURSEDpar(:,:)  ! dimension(mMax*iMaxSURSED,nParMax) forcing parameters for vertLoc=WAT cells, we need cell height and water column height
    integer,      intent(in)     :: whichArray(:,:)      ! dimension(xLength, kMax) which of the four arrays before we need, 0=land point
    integer,      intent(in)     :: arrayIndex(:,:)      ! dimension(xLength, kMax) index in the euler*par array
    

    integer :: j, k        ! indexes for destination grid dimension 2 and 3
    integer :: jk          ! destination grid dimension 2 and 3 combined index
    integer :: mn          ! destination grid dimension 1
    integer :: wa          ! local whichArray value
    real    :: heightRatio ! ratio between cell height and water column height
    
    jk   = blockDim%x * (blockIdx%x - 1) + threadIdx%x
    k     = (jk-1)/xLength+1
    j    = MOD(jk-1,xLength)+1
    
    if (k .le. kMax) then
      mn = mMax*nVarMax + 1 ! save runoff directly after BGC variables
      ! calculate the heightRatio between cell height and seafloor depth
      ! cellheight is stored in nPar=5 and seafloordepth in nPar=6
      heightRatio = 0.0
      wa = whichArray(j,k) 
      if (wa .gt. 0) then
        if (wa .eq. 1) then
          heightRatio = eulerWATpar(arrayIndex(j,k)*mMax, 5) / max(eulerWATpar(arrayIndex(j,k)*mMax, 6), 1.0e-10)
        end if
        if (wa .eq. 2) then
          heightRatio = eulerSURpar(arrayIndex(j,k)*mMax, 5) / max(eulerSURpar(arrayIndex(j,k)*mMax, 6), 1.0e-10)
        end if
        if (wa .eq. 3) then
          heightRatio = eulerSEDpar(arrayIndex(j,k)*mMax, 5) / max(eulerSEDpar(arrayIndex(j,k)*mMax, 6), 1.0e-10)
        end if
        if (wa .eq. 4) then
          heightRatio = eulerSURSEDpar(arrayIndex(j,k)*mMax, 5) / max(eulerSURSEDpar(arrayIndex(j,k)*mMax, 6), 1.0e-10)
        end if
        myArray(mn, j, k) = inArray(j) * heightRatio
      end if
    end if
  end subroutine transpose_obc_x2jk_runoff_single

  attributes(global) launch_bounds(CUDA_TPB_TRANSPOSE_ORIG2VB, CUDA_BPM_TRANSPOSE_ORIG2VB) &
                     subroutine transpose_obc_xyz2jk_single(inArray, myArray, n, xLength, yLength, kMaxOBC, kMax, mMax, nVarMax, mnPadding)
  ! This subroutine will copy the variables stored in one vertical block to the
  ! appropriate memory locations for the Eulerian gridpoints
    implicit none
    ! the following four indexes all have dimension(kMax):
    real(kind=4), intent(in)     :: inArray(:,:,:)       ! dimension(xLength, yLength, kMaxOBC) values as read from file
    real,         intent(inout)  :: myArray(:,:,:)       ! dimension(mnMax, xLength*yLength, kMax) output array
    integer, value               :: n                    ! variable index
    integer, value               :: xLength              ! original grid dimension 1
    integer, value               :: yLength              ! original grid dimension 2
    integer, value               :: kMaxOBC              ! original grid dimension 3
    integer, value               :: kMax                 ! destination grid dimension 3
    integer, value               :: mMax                 ! number of ensemble members
    integer, value               :: nVarMax              ! number of state variables
    integer, value               :: mnPadding            ! space holder for alignment
    

    integer :: m, j, k     ! indexes for ensemble member, destination grid dimension 2, and destination grid dimension 3
    integer :: mj          ! ensemble member and destination grid dimension 2 combined index
    integer :: mjk         ! ensemble member and destination grid dimension 2 and 3 combined index
    integer :: ii, jj      ! original grid dimension 1 and 2 index
    integer :: mn          ! destination grid dimension 1
    
    mjk   = blockDim%x * (blockIdx%x - 1) + threadIdx%x
    k     = (mjk-1)/(mMax*xLength*yLength)+1
    mj    = MOD(mjk-1,mMax*xLength*yLength)+1
    j     = (mj-1)/mMax+1
    m     = MOD(mj-1,mMax)+1
    jj    = (j-1)/xLength+1
    ii    = MOD(j-1,xLength)+1
    
    if ((k .le. kMaxOBC) .and. (k .le. kMax)) then
      mn = m + (n-1)*mMax
      myArray(mn, j, k) = inArray(ii,jj,k)
    end if
  end subroutine transpose_obc_xyz2jk_single

end module transpose_kernel_obc_river
