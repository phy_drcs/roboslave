module main_init_mod

contains

subroutine main_init
! This subroutine does the entire initialization
  use variables_mod
  use timers_mod
  use time_conversion_mod
  use init_grid_mod,          only : init_grid
  use init_advection_mod,     only : init_advection
  use init_allocate_mod,      only : init_allocate
  use init_bgc_constants_mod, only : init_bgc_constants
  use init_bgc_variables_mod, only : init_bgc_variables
  use init_bgc_variables_sedi_mod, only : init_bgc_variables_sedi
  use init_bgc_obc_mod,       only : init_bgc_obc, init_bgc_no_obc
  use init_bgc_river_mod,     only : init_bgc_river, init_bgc_no_river
  use init_forcing_mod,       only : init_forcing, convert_forcing
  use init_transfer_mod,      only : init_transfer
  use init_output_mod,        only : init_output, init_output_sedi
  use restart_mod,            only : read_restart
  use sediment_utils_mod
  use cudafor
  implicit none

  integer(kind=4) :: istat
  integer         :: namelistUnit, jBlock

  ! read in namelist
  open (action='read', file="input/input.nml", newunit=namelistUnit)
  read (nml=input, unit=namelistUnit)
  close(namelistUnit)

  timestep = 1

  ! complete time information
  dateTime = string_to_datetime(startDate)                                   ! set current date (in Windows time format) to start date
  write(*,*) "    initial date: " // datetime_to_string(dateTime); call flush(6)
  if (dt .eq. 0) dt = 1.0/timestepsPerDay                                    ! calculate length of the time step, if not given 
  if (timesteps .eq. 0) timesteps = NINT(days / dt)                          ! calculate number of time steps, if not given 
  if (outputInterval .eq. 0) outputInterval = NINT(outputIntervalDays / dt)  ! calculate output interval in time steps, if not given

  ! define number of fields to save for each vertLoc=WAT variable 
  !   (nVarFields=2 is the default, store concentration and vertical velocity)
  !   (nVarFields=3 if an additional vertical diffusivity exists for some of the state variables)
  nVarFields = 2
  if (useVertDiff) nVarFields = 3

  ! activate or deactivate 256-byte memory alignment in the drifter array
  if (slowerToSaveVRAM .ge. 1) then
    mnPadding = 1
  else
    mnPadding = 256/kind(1.0)+1-mod(mMax*nvarMax+1,256/kind(1.0))
  end if

  ! reading grid information
  call start_timer(timer_init_grid)
  write(*,*) "  loading grid information ..."; call flush(6)
  call init_grid
  call stop_timer(timer_init_grid)

  ! reading advection information
  call start_timer(timer_init_advection)
  write(*,*) "  loading advection information ..."; call flush(6)
  call init_advection
  call stop_timer(timer_init_advection)

  ! check whether dimensions are inconsistent
  ! in that case print suggestion for changing variables.CUF and stop
  if ((iMaxWAT    .ne. iMaxWATNew   ) .or. &
      (iMaxSUR    .ne. iMaxSURNew   ) .or. &
      (iMaxSED    .ne. iMaxSEDNew   ) .or. &
      (iMaxSURSED .ne. iMaxSURSEDNew) .or. &
      (jMax       .ne. jMaxNew      ) .or. &
      (kMax       .ne. kMaxNew      ) .or. &
      (lMax       .ne. lMaxNew      )) then
    write(*,*) "FATAL: Dimensions of input files do not match those given in src/variables.CUF."
    write(*,*) "       Please change src/variables.CUF to the following values:"
    write(*,'(a,i16,a)') "         integer, parameter :: iMaxWAT    = ",iMaxWATNew,   "  ! number of interior water cells"
    write(*,'(a,i16,a)') "         integer, parameter :: iMaxSUR    = ",iMaxSURNew,   "  ! number of surface water cells"
    write(*,'(a,i16,a)') "         integer, parameter :: iMaxSED    = ",iMaxSEDNew,   "  ! number of bottom water cells"
    write(*,'(a,i16,a)') "         integer, parameter :: iMaxSURSED = ",iMaxSURSEDNew,"  ! number of water cells with contact to surface and bottom (1 vert. layer)"
    write(*,'(a,i16,a)') "         integer, parameter :: jMax       = ",jMaxNew,      "  ! number of surface water points"
    write(*,'(a,i16,a)') "         integer, parameter :: kMax       = ",kMaxNew,      "  ! number of vertical levels"
    write(*,'(a,i16,a)') "         integer, parameter :: lMax       = ",lMaxNew,      "  ! number of Lagrangian drifters per grid cell (upper limit)"
    write(*,'(a,i16,a)') "         integer, parameter :: mMax       = ",mMaxNew,      "  ! number of ensemble members"
    write(*,'(a,i16,a)') "         integer, parameter :: nparMax    = ",nparMaxNew,   "  ! number of physical parameters" 
    write(*,*) "       Then, build and run again."
    stop
  end if

  ! allocate big arrays
  call start_timer(timer_init_allocate)
  write(*,*) "  allocating arrays ..."; call flush(6)
  call init_allocate
  call stop_timer(timer_init_allocate)
  
  ! initialize BGC constants
  call start_timer(timer_init_bgc)
  write(*,*) "  setting BGC constants ..."; call flush(6)
  call init_bgc_constants

  ! initialize BGC variables
  write(*,*) "  initializing BGC state variables ..."; call flush(6)
  call init_bgc_variables

  if (kMaxSedi > 0) then
    ! initialize sediment grid
    write(*,*) "  initializing sediment grid ..."
    call flush(6)
    call init_grid_sedi
    write(*,*) "  sediment grid initialization finalized."
    call flush(6)

    select case (diffMethodSedi)
      case ('central')
        finiteDifferenceMethod = CENTRAL_DIFF
      case ('upwind')
        finiteDifferenceMethod = UPWIND_DIFF
      case ('hybrid')
        finiteDifferenceMethod = HYBRID_DIFF
      case ('patankar')
        finiteDifferenceMethod = PATANKAR_DIFF
      case default
        finiteDifferenceMethod = HYBRID_DIFF
    end select

    select case (tortuosityEquation)
      case ('MOD_WEISSBERG_TORT')
        tortuosityRelationship = MOD_WEISSBERG_TORT
      case ('ARCHIE_TORT')
        tortuosityRelationship = ARCHIE_TORT
      case default
        tortuosityRelationship = MOD_WEISSBERG_TORT
    end select

    call init_bgc_variables_sedi

    ! copy diffusivities indices already to the gpu
    sedimentGrid%diffusivityID = sedimentGrid%diffusivityID_h
  end if

  
  ! initialize BGC OBC
  if (obcFile .eq. '') then
    call init_bgc_no_obc
  else
    write(*,*) "  initializing BGC open boundary conditions ..."; call flush(6)
    call init_bgc_obc
  end if

  ! initialize BGC River
  if (trim(riverFile) .eq. '') then
    call init_bgc_no_river
  else
    write(*,*) "  initializing BGC river loads ..."; call flush(6)
    call init_bgc_river
  end if
  call stop_timer(timer_init_bgc)

 
  ! initialize forcing variables
  call start_timer(timer_init_forcing)
  write(*,*) "  initializing physical forcing ..."; call flush(6)
  call init_forcing
  call stop_timer(timer_init_forcing)

  ! convert forcing files if this is desired in the namelist
  if (convertForcing) then
    call convert_forcing
  end if

  ! initialize opacity
  ! TODO do a dummy BGC timestep to get initial opacity, then remove opacity from init_transfer
  do jBlock=1,jBlocks
    vBlocks(jBlock)%opacity_h = 0.0
  end do

  ! initialize output arrays
  eulerWAT%output    = 0.0
  eulerSUR%output    = 0.0
  eulerSED%output    = 0.0
  eulerSURSED%output = 0.0

  ! if applicable, load a restart file
  ! TODO change name of restart file if it contains wildcards
  if (trim(readRestartFile) .ne. "") then
    call read_restart
  end if
  
  ! transfer initial arrays to device
  call start_timer(timer_init_transfer)
  write(*,*) "  copy initial data to GPU ..."; call flush(6)
  call init_transfer

  ! wait until data transfer has happened
  istat = cudaDeviceSynchronize()
  call stop_timer(timer_init_transfer)

  ! initialize output file
  ! TODO change name of output file if it contains wildcards
  write(*,*) "  initializing output file ..."; call flush(6)
  call init_output
  if (outputStyleSedi .eq. 'sediblock') then
    write(*,*) "  initializing sediment output file ..."
    call init_output_sedi
  end if

end subroutine main_init
end module main_init_mod

