module init_bgc_variables_mod
contains

  subroutine init_bgc_variables
  ! This subroutine sets the values of the biogeochemical vaiables used in the model
    use init_bgc_singlevariable_mod
    use variables_mod
    use cudafor
    implicit none
    integer            :: n
    character(len=255) :: s
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! use given initValue property !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! vertLoc=WAT tracers
    real, parameter :: initValue_of_t_sul           = 0.0 ! sulfur
    real, parameter :: initValue_of_t_doc           = 0.0 ! dissolved organic carbon
    real, parameter :: initValue_of_t_dop           = 0.0 ! phosphorus in dissolved organic carbon in Redfield ratio
    real, parameter :: initValue_of_t_don           = 0.0 ! nitrogen in dissolved organic carbon in Redfield ratio
    real, parameter :: initValue_of_t_cdom          = 0.0 ! colored dissolved organic carbon
    real, parameter :: initValue_of_t_poc           = 0.0 ! particulate organic carbon
    real, parameter :: initValue_of_t_pocp          = 0.0 ! phosphorus in particulate organic carbon in Redfield ratio
    real, parameter :: initValue_of_t_pocn          = 0.0 ! nitrogen in particulate organic carbon in Redfield ratio
    real, parameter :: initValue_of_t_n2            = -1e20
    real, parameter :: initValue_of_t_o2            = -1e20
    real, parameter :: initValue_of_t_dic           = -1e20
    real, parameter :: initValue_of_t_nh4           = -1e20
    real, parameter :: initValue_of_t_no3           = -1e20
    real, parameter :: initValue_of_t_po4           = -1e20
    real, parameter :: initValue_of_t_spp           = -1e20
    real, parameter :: initValue_of_t_zoo           = -1e20
    real, parameter :: initValue_of_t_h2s           = -1e20
    real, parameter :: initValue_of_t_alk           = -1e20
    real, parameter :: initValue_of_t_lip           = -1e20
    real, parameter :: initValue_of_t_cya           = -1e20
    real, parameter :: initValue_of_t_det           = -1e20
    real, parameter :: initValue_of_t_lpp           = -1e20
    real, parameter :: initValue_of_t_ipw           = -1e20

    ! vertLoc=SED tracers
    real, parameter :: initValue_of_t_sed           = -1e20
    real, parameter :: initValue_of_t_ips           = -1e20
    real, parameter :: initValue_of_t_sed_poc       = -1e20
    real, parameter :: initValue_of_t_sed_pocn      = -1e20
    real, parameter :: initValue_of_t_sed_pocp      = -1e20

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! call a function to check if the variable exists in an initial file, this has priority !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! vertLoc=WAT tracers
    write(*,*) "    tracers with vertLoc=WAT:"; call flush(6)
    n=1
    s = trim("t_n2           ")
    call init_bgc_singlevariable(n, s, initValue_of_t_n2           , .false.) ; n=n+1
    s = trim("t_o2           ")
    call init_bgc_singlevariable(n, s, initValue_of_t_o2           , .false.) ; n=n+1
    s = trim("t_dic          ")
    call init_bgc_singlevariable(n, s, initValue_of_t_dic          , .false.) ; n=n+1
    s = trim("t_nh4          ")
    call init_bgc_singlevariable(n, s, initValue_of_t_nh4          , .false.) ; n=n+1
    s = trim("t_no3          ")
    call init_bgc_singlevariable(n, s, initValue_of_t_no3          , .false.) ; n=n+1
    s = trim("t_po4          ")
    call init_bgc_singlevariable(n, s, initValue_of_t_po4          , .false.) ; n=n+1
    s = trim("t_spp          ")
    call init_bgc_singlevariable(n, s, initValue_of_t_spp          , .false.) ; n=n+1
    s = trim("t_zoo          ")
    call init_bgc_singlevariable(n, s, initValue_of_t_zoo          , .false.) ; n=n+1
    s = trim("t_h2s          ")
    call init_bgc_singlevariable(n, s, initValue_of_t_h2s          , .false.) ; n=n+1
    s = trim("t_sul          ")
    call init_bgc_singlevariable(n, s, initValue_of_t_sul          , .false.) ; n=n+1
    s = trim("t_alk          ")
    call init_bgc_singlevariable(n, s, initValue_of_t_alk          , .false.) ; n=n+1
    s = trim("t_lip          ")
    call init_bgc_singlevariable(n, s, initValue_of_t_lip          , .false.) ; n=n+1
    s = trim("t_doc          ")
    call init_bgc_singlevariable(n, s, initValue_of_t_doc          , .false.) ; n=n+1
    s = trim("t_dop          ")
    call init_bgc_singlevariable(n, s, initValue_of_t_dop          , .false.) ; n=n+1
    s = trim("t_don          ")
    call init_bgc_singlevariable(n, s, initValue_of_t_don          , .false.) ; n=n+1
    s = trim("t_cdom         ")
    call init_bgc_singlevariable(n, s, initValue_of_t_cdom         , .false.) ; n=n+1
    s = trim("t_cya          ")
    call init_bgc_singlevariable(n, s, initValue_of_t_cya          , .false.) ; n=n+1
    s = trim("t_det          ")
    call init_bgc_singlevariable(n, s, initValue_of_t_det          , .false.) ; n=n+1
    s = trim("t_poc          ")
    call init_bgc_singlevariable(n, s, initValue_of_t_poc          , .false.) ; n=n+1
    s = trim("t_pocp         ")
    call init_bgc_singlevariable(n, s, initValue_of_t_pocp         , .false.) ; n=n+1
    s = trim("t_pocn         ")
    call init_bgc_singlevariable(n, s, initValue_of_t_pocn         , .false.) ; n=n+1
    s = trim("t_lpp          ")
    call init_bgc_singlevariable(n, s, initValue_of_t_lpp          , .false.) ; n=n+1
    s = trim("t_ipw          ")
    call init_bgc_singlevariable(n, s, initValue_of_t_ipw          , .false.) ; n=n+1
    write(*,*) "    finished initializing tracers with vertLoc=WAT."; call flush(6)

    ! vertLoc=SED tracers
    write(*,*) "    tracers with vertLoc=SED:"; call flush(6)
    n=1
    s = trim("t_sed          ")
    call init_bgc_singlevariable(n, s, initValue_of_t_sed          , .true.) ; n=n+1
    s = trim("t_ips          ")
    call init_bgc_singlevariable(n, s, initValue_of_t_ips          , .true.) ; n=n+1
    s = trim("t_sed_poc      ")
    call init_bgc_singlevariable(n, s, initValue_of_t_sed_poc      , .true.) ; n=n+1
    s = trim("t_sed_pocn     ")
    call init_bgc_singlevariable(n, s, initValue_of_t_sed_pocn     , .true.) ; n=n+1
    s = trim("t_sed_pocp     ")
    call init_bgc_singlevariable(n, s, initValue_of_t_sed_pocp     , .true.) ; n=n+1
    write(*,*) "    finished initializing tracers with vertLoc=SED."; call flush(6)

  end subroutine init_bgc_variables
end module init_bgc_variables_mod
