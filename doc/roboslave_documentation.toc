\contentsline {section}{\numberline {1}Introduction}{5}{section.1}%
\contentsline {subsection}{\numberline {1.1}What is roboslave}{5}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}What do I need to run it}{5}{subsection.1.2}%
\contentsline {subsection}{\numberline {1.3}Contents of this document}{6}{subsection.1.3}%
\contentsline {section}{\numberline {2}Running the first example}{7}{section.2}%
\contentsline {subsection}{\numberline {2.1}Installing the prerequisites}{7}{subsection.2.1}%
\contentsline {subsubsection}{\numberline {2.1.1}Finding out if you have an NVIDIA GPU installed}{7}{subsubsection.2.1.1}%
\contentsline {subsubsection}{\numberline {2.1.2}Installing the corresponding NVIDIA driver}{7}{subsubsection.2.1.2}%
\contentsline {subsubsection}{\numberline {2.1.3}Under Windows: Installing WSL2 and Ubuntu}{7}{subsubsection.2.1.3}%
\contentsline {subsubsection}{\numberline {2.1.4}Installing the CUDA toolkit}{8}{subsubsection.2.1.4}%
\contentsline {subsubsection}{\numberline {2.1.5}Installing the NVIDIA HPC Software Development toolkit}{8}{subsubsection.2.1.5}%
\contentsline {subsubsection}{\numberline {2.1.6}Getting the code generation tool (cgt) executables}{8}{subsubsection.2.1.6}%
\contentsline {subsection}{\numberline {2.2}Installing roboslave}{9}{subsection.2.2}%
\contentsline {subsubsection}{\numberline {2.2.1}Getting roboslave source code}{9}{subsubsection.2.2.1}%
\contentsline {subsubsection}{\numberline {2.2.2}Building the netCDF library with the NVIDIA compilers}{9}{subsubsection.2.2.2}%
\contentsline {subsubsection}{\numberline {2.2.3}Adjusting your build script}{9}{subsubsection.2.2.3}%
\contentsline {subsection}{\numberline {2.3}Building roboslave}{10}{subsection.2.3}%
\contentsline {subsection}{\numberline {2.4}Obtaining example forcing data}{10}{subsection.2.4}%
\contentsline {subsection}{\numberline {2.5}Running roboslave}{11}{subsection.2.5}%
\contentsline {section}{\numberline {3}Configuration and input}{12}{section.3}%
\contentsline {subsection}{\numberline {3.1}input.nml}{12}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Providing the model grid}{12}{subsection.3.2}%
\contentsline {subsubsection}{\numberline {3.2.1}gridStyle = MOM5}{12}{subsubsection.3.2.1}%
\contentsline {subsubsection}{\numberline {3.2.2}gridStyle = MOM6}{13}{subsubsection.3.2.2}%
\contentsline {subsection}{\numberline {3.3}Providing initial data}{13}{subsection.3.3}%
\contentsline {subsubsection}{\numberline {3.3.1}initStyle = MOM5}{13}{subsubsection.3.3.1}%
\contentsline {subsection}{\numberline {3.4}Providing physical forcing}{14}{subsection.3.4}%
\contentsline {subsubsection}{\numberline {3.4.1}forcingStyle = MOM5}{15}{subsubsection.3.4.1}%
\contentsline {subsubsection}{\numberline {3.4.2}forcingStyle = MOM6}{15}{subsubsection.3.4.2}%
\contentsline {subsubsection}{\numberline {3.4.3}forcingStyle = fast}{16}{subsubsection.3.4.3}%
\contentsline {subsection}{\numberline {3.5}Providing transport (advection) information}{16}{subsection.3.5}%
\contentsline {subsection}{\numberline {3.6}Providing river information}{17}{subsection.3.6}%
\contentsline {subsection}{\numberline {3.7}Providing OBC (open boundary) data}{18}{subsection.3.7}%
\contentsline {subsection}{\numberline {3.8}Providing model constants}{18}{subsection.3.8}%
\contentsline {section}{\numberline {4}Details on the hybrid Euler-Lagrange scheme}{19}{section.4}%
\contentsline {subsection}{\numberline {4.1}Philosophy of the PCPM method}{19}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Volume conservation}{19}{subsection.4.2}%
\contentsline {subsubsection}{\numberline {4.2.1}What does volume conservation mean in a Lagrangian perspective?}{19}{subsubsection.4.2.1}%
\contentsline {subsubsection}{\numberline {4.2.2}Simple test cases}{20}{subsubsection.4.2.2}%
\contentsline {subsubsection}{\numberline {4.2.3}Which schemes are volume conserving?}{20}{subsubsection.4.2.3}%
\contentsline {subsubsection}{\numberline {4.2.4}Volume conservation in Lat-Lon coordinates}{22}{subsubsection.4.2.4}%
\contentsline {subsection}{\numberline {4.3}Fluxes across boundaries}{22}{subsection.4.3}%
\contentsline {subsubsection}{\numberline {4.3.1}General considerations}{22}{subsubsection.4.3.1}%
\contentsline {subsubsection}{\numberline {4.3.2}Open boundaries}{24}{subsubsection.4.3.2}%
\contentsline {subsubsection}{\numberline {4.3.3}Rivers}{24}{subsubsection.4.3.3}%
\contentsline {subsubsection}{\numberline {4.3.4}Surface volume fluxes}{25}{subsubsection.4.3.4}%
\contentsline {subsubsection}{\numberline {4.3.5}Surface tracer fluxes}{25}{subsubsection.4.3.5}%
\contentsline {subsubsection}{\numberline {4.3.6}Numerically induced particle creation / destruction}{26}{subsubsection.4.3.6}%
\contentsline {section}{\numberline {5}Technical implementation in CUDA Fortran}{27}{section.5}%
\contentsline {subsection}{\numberline {5.1}Storage of variables and forcing in memory}{27}{subsection.5.1}%
\contentsline {subsubsection}{\numberline {5.1.1}Eulerian arrays (for BGC calculations)}{27}{subsubsection.5.1.1}%
\contentsline {subsubsection}{\numberline {5.1.2}Vertical blocks (for vertical mixing, vertical movement and light calculation)}{29}{subsubsection.5.1.2}%
\contentsline {subsubsection}{\numberline {5.1.3}Lagrangian drifters (for advection)}{29}{subsubsection.5.1.3}%
\contentsline {subsection}{\numberline {5.2}Required sub-steps of a single time step}{30}{subsection.5.2}%
\contentsline {subsection}{\numberline {5.3}Technical details on loading physical forcing}{31}{subsection.5.3}%
\contentsline {subsubsection}{\numberline {5.3.1}Philosophy of regridding}{31}{subsubsection.5.3.1}%
\contentsline {subsubsection}{\numberline {5.3.2}Triple-buffered scheme}{32}{subsubsection.5.3.2}%
\contentsline {subsection}{\numberline {5.4}Kernels for different tasks}{33}{subsection.5.4}%
\contentsline {subsubsection}{\numberline {5.4.1}BGC kernels}{33}{subsubsection.5.4.1}%
\contentsline {subsubsection}{\numberline {5.4.2}Light kernel}{33}{subsubsection.5.4.2}%
\contentsline {subsubsection}{\numberline {5.4.3}Vertical diffusion and advection kernel}{33}{subsubsection.5.4.3}%
\contentsline {subsubsection}{\numberline {5.4.4}Updating the concentrations on drifters kernel}{35}{subsubsection.5.4.4}%
\contentsline {subsubsection}{\numberline {5.4.5}Passive advection of drifters kernel}{36}{subsubsection.5.4.5}%
\contentsline {section}{\numberline {6}Tools that come with roboslave}{37}{section.6}%
\contentsline {subsection}{\numberline {6.1}gpu2zax}{37}{subsection.6.1}%
\contentsline {subsubsection}{\numberline {6.1.1}Input and output files}{37}{subsubsection.6.1.1}%
\contentsline {subsubsection}{\numberline {6.1.2}Procedure of regridding}{37}{subsubsection.6.1.2}%
\contentsline {subsubsection}{\numberline {6.1.3}Vertical regridding options}{37}{subsubsection.6.1.3}%
\contentsline {subsubsection}{\numberline {6.1.4}Horizontal regridding options}{38}{subsubsection.6.1.4}%
\contentsline {subsubsection}{\numberline {6.1.5}Other regridding options}{38}{subsubsection.6.1.5}%
\contentsline {subsection}{\numberline {6.2}roboadvect}{38}{subsection.6.2}%
\contentsline {subsubsection}{\numberline {6.2.1}Providing input data}{38}{subsubsection.6.2.1}%
\contentsline {subsubsection}{\numberline {6.2.2}Initial locations of particles}{39}{subsubsection.6.2.2}%
\contentsline {subsubsection}{\numberline {6.2.3}Lagrangian transport schemes}{39}{subsubsection.6.2.3}%
\contentsline {subsubsection}{\numberline {6.2.4}Creation and destruction of particles}{40}{subsubsection.6.2.4}%
\contentsline {section}{\numberline {7}Running the second example}{41}{section.7}%
\contentsline {subsection}{\numberline {7.1}Obtaining the output files of the hydrodynamic model run}{41}{subsection.7.1}%
\contentsline {subsection}{\numberline {7.2}Changing internal NetCDF format}{41}{subsection.7.2}%
\contentsline {subsection}{\numberline {7.3}Transforming to z-coordinates}{41}{subsection.7.3}%
\contentsline {subsection}{\numberline {7.4}Creating Lagrangian trajectories}{42}{subsection.7.4}%
\providecommand \tocbasic@end@toc@file {}\tocbasic@end@toc@file 
