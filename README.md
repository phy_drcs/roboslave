# roboslave

rapid ocean biogeochemistry offline simulation by lagrangian advection of vectors

<img src="./doc/roboslave.png" alt="roboslave logo" width="200em"/>

Please see the quickstart guide in Section 2 of [doc/roboslave_documentation.pdf](doc/roboslave_documentation.pdf)
