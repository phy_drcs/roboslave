include_flags="-I ${basedir}/compiled"
#normal mode
#biogeochemistry code files use lower optimization because the code files can become too complex and produce errors when optimized heavily
fflags=-mcmodel=medium -O1 -i8 -r8 -gpu=cc${cuda_compute_capability},keep ${netcdf_fflags} -Minfo=all -Kieee -Mnofma
fflagsbgc=-mcmodel=medium -O1 -i8 -r8 -gpu=cc${cuda_compute_capability},keep ${netcdf_fflags} -Minfo=all -Kieee -Mnofma
ldflags=-Xlinker -rpath -Xlinker ${netcdf_libpath}/ ${extra_ldflags} -gpu=cc${cuda_compute_capability} -Minfo=all -Kieee -Mnofma
#debug mode
#fflags=-mcmodel=medium -i8 -r8 -gpu=cc${cuda_compute_capability},keep ${netcdf_fflags} -g -Mchkptr -Mchkstk -Mbounds -Minit-integer=1234567 -Minit-real=snan -Ktrap=fp -lineinfo -traceback  
#fflagsbgc=-mcmodel=medium -i8 -r8 -gpu=cc${cuda_compute_capability},keep ${netcdf_fflags} -g -Mchkptr -Mchkstk -Mbounds -Minit-integer=1234567 -Minit-real=snan -Ktrap=fp -lineinfo -traceback
#ldflags=-Xlinker -rpath -Xlinker ${netcdf_libpath}/ ${extra_ldflags} -gpu=cc${cuda_compute_capability} -g -Mchkptr -Mchkstk -Mbounds -Minit-integer=1234567 -Minit-real=snan -Ktrap=fp -traceback
fc=nvfortran

#########################
# disable default rules #
#########################
.SUFFIXES:

################################
# main rule to make executable #
################################
roborun: \
	compiled/main.o 
		cd compiled; rm -f ../roborun; ${fc}  -cuda=charstring ${netcdf_ldflags} ${ldflags} -o ../roborun *.o; cd ..

###########################
# creating model code ... #
###########################
src/bgc_dimensions.cuf \
src/bgc_kernel_WAT.CUF \
src/bgc_kernel_SUR.CUF \
src/bgc_kernel_SED.CUF \
src/bgc_kernel_SURSED.CUF \
src/init_bgc_constants.cuf \
src/init_bgc_variables.cuf \
src/init_bgc_output.cuf \
src/bgc_dimensions_sedi.cuf \
src/init_bgc_variables_sedi.cuf \
src/bgc_kernel_sedi.CUF \
src/init_bgc_output_sedi.cuf &: \
	cgt/code_templates/bgc_dimensions.cuf \
	cgt/code_templates/bgc_kernel_WAT.CUF \
	cgt/code_templates/bgc_kernel_SUR.CUF \
	cgt/code_templates/bgc_kernel_SED.CUF \
	cgt/code_templates/bgc_kernel_SURSED.CUF \
	cgt/code_templates/init_bgc_constants.cuf \
	cgt/code_templates/init_bgc_variables.cuf \
	cgt/code_templates/init_bgc_obc.cuf \
	cgt/code_templates/init_bgc_river.cuf \
	cgt/code_templates/init_bgc_output.cuf \
	cgt/textfiles/modelinfos.txt \
	cgt/textfiles/constants.txt \
	cgt/textfiles/tracers.txt \
	cgt/textfiles/auxiliaries.txt \
	cgt/textfiles/processes.txt \
	cgt/textfiles/elements.txt \
	cgt/textfiles/celements.txt \
	cgt/code_templates_sedi/bgc_dimensions_sedi.cuf \
	cgt/code_templates_sedi/init_bgc_variables_sedi.cuf \
	cgt/code_templates_sedi/init_bgc_output_sedi.cuf \
	cgt/code_templates_sedi/bgc_kernel_sedi.CUF \
	cgt/textfiles_sedi/modelinfos.txt \
	cgt/textfiles_sedi/constants.txt \
	cgt/textfiles_sedi/tracers.txt \
	cgt/textfiles_sedi/auxiliaries.txt \
	cgt/textfiles_sedi/processes.txt \
	cgt/textfiles_sedi/elements.txt \
	cgt/textfiles_sedi/celements.txt 
		cgt -t ${basedir}/cgt/code_templates -o ${basedir}/src ${basedir}/cgt/textfiles/modelinfos.txt; cgt -t ${basedir}/cgt/code_templates_sedi -o ${basedir}/src ${basedir}/cgt/textfiles_sedi/modelinfos.txt

###########################
# creating sediment model code ... #
###########################
# src/bgc_dimensions_sedi.cuf \
# src/init_bgc_variables_sedi.cuf \
# src/bgc_kernel_sedi.CUF \
# src/init_bgc_output_sedi.cuf &: \
# 	cgt/code_templates_sedi/bgc_dimensions_sedi.cuf \
# 	cgt/code_templates_sedi/init_bgc_variables_sedi.cuf \
# 	cgt/code_templates_sedi/init_bgc_output_sedi.cuf \
# 	cgt/code_templates_sedi/bgc_kernel_sedi.CUF \
# 	cgt/textfiles_sedi/modelinfos.txt \
# 	cgt/textfiles_sedi/constants.txt \
# 	cgt/textfiles_sedi/tracers.txt \
# 	cgt/textfiles_sedi/auxiliaries.txt \
# 	cgt/textfiles_sedi/processes.txt \
# 	cgt/textfiles_sedi/elements.txt \
# 	cgt/textfiles_sedi/celements.txt 
# 		cgt -t ${basedir}/cgt/code_templates_sedi -o ${basedir}/src ${basedir}/cgt/textfiles_sedi/modelinfos.txt

#############################
# copying include files ... #
#############################
compiled/cuda_settings.inc: src/cuda_settings.inc
		cp src/cuda_settings.inc compiled/cuda_settings.inc

##################################################################
# compiling variable definitions and general helper routines ... #
##################################################################
compiled/bgc_dimensions_mod.mod: \
	src/bgc_dimensions.cuf 
		cd compiled; rm -f bgc_dimensions_mod.mod; ${fc} ${fflags} -c ../src/bgc_dimensions.cuf; cd ..

compiled/bgc_dimensions_sedi_mod.mod: \
	src/bgc_dimensions_sedi.cuf 
		cd compiled; rm -f bgc_dimensions_sedi_mod.mod; ${fc} ${fflags} -c ../src/bgc_dimensions_sedi.cuf; cd ..

compiled/types_mod.mod: \
	compiled/cuda_settings.inc compiled/bgc_dimensions_mod.mod src/types.CUF 
		cd compiled; rm -f types_mod.mod; ${fc} ${fflags} -c ../src/types.CUF; cd ..

compiled/variables_mod.mod: \
	compiled/cuda_settings.inc compiled/bgc_dimensions_mod.mod compiled/types_mod.mod src/variables.CUF
		cd compiled; rm -f variables_mod.mod; ${fc} ${fflags} -c ../src/variables.CUF; cd ..

compiled/timers_mod.mod: \
	src/timers.cuf
		cd compiled; rm -f timers_mod.mod; ${fc} ${fflags} -c ../src/timers.cuf; cd ..

compiled/time_conversion_mod.mod: \
	src/time_conversion.cuf 
		cd compiled; rm -f time_conversion_mod.mod; ${fc} ${fflags} -c ../src/time_conversion.cuf; cd ..

compiled/cuda_check_mod.mod: \
	src/cuda_check.cuf 
		cd compiled; rm -f cuda_check_mod.mod; ${fc} ${fflags} -c ../src/cuda_check.cuf; cd ..

compiled/sediment_utils.mod: \
	compiled/types_mod.mod \
	compiled/bgc_dimensions_sedi_mod.mod \
	compiled/variables_mod.mod \
	compiled/netcdf_check_mod.mod \
	src/sediment_utils.cuf
		cd compiled; rm -f sediment_utils.mod; ${fc} ${fflagsbgc} -c ../src/sediment_utils.cuf; cd ..


#############################
# compiling bgc kernels ... #
#############################
compiled/bgc_kernel_wat.mod: \
	compiled/cuda_settings.inc src/bgc_kernel_WAT.CUF 
		cd compiled; rm -f bgc_kernel_wat.mod; ${fc} ${fflagsbgc} -c ../src/bgc_kernel_WAT.CUF; cd ..

compiled/bgc_kernel_sur.mod: \
	compiled/cuda_settings.inc src/bgc_kernel_SUR.CUF 
		cd compiled; rm -f bgc_kernel_sur.mod; ${fc} ${fflagsbgc} -c ../src/bgc_kernel_SUR.CUF; cd ..

compiled/bgc_kernel_sed.mod: \
	compiled/cuda_settings.inc src/bgc_kernel_SED.CUF 
		cd compiled; rm -f bgc_kernel_sed.mod; ${fc} ${fflagsbgc} -c ../src/bgc_kernel_SED.CUF; cd ..

compiled/bgc_kernel_sursed.mod: \
	compiled/cuda_settings.inc src/bgc_kernel_SURSED.CUF 
		cd compiled; rm -f bgc_kernel_sursed.mod; ${fc} ${fflagsbgc} -c ../src/bgc_kernel_SURSED.CUF; cd ..

##############################
# compiling light kernel ... #
##############################
compiled/light_kernel.mod: \
	compiled/cuda_settings.inc src/light_kernel.CUF 
		cd compiled; rm -f light_kernel.mod; ${fc} ${fflags} -c ../src/light_kernel.CUF; cd ..

###########################################
# compiling vertical diffusion kernel ... #
###########################################
compiled/vertdiff_kernel.mod: \
	compiled/cuda_settings.inc src/vertdiff_kernel.CUF \
	compiled/bgc_kernel_wat.mod
		cd compiled; rm -f vertdiff_kernel.mod; ${fc} ${fflags} -c ../src/vertdiff_kernel.CUF; cd ..

##################################
# compiling advection kernel ... #
##################################
compiled/advection_kernel.mod: \
	compiled/cuda_settings.inc src/advection_kernel.CUF 
		cd compiled; rm -f advection_kernel.mod; ${fc} ${fflagsbgc} -c ../src/advection_kernel.CUF; cd ..

###################################
# compiling transpose kernels ... #
###################################
compiled/transpose_kernel_euler2vb.mod: \
	src/transpose_kernel_euler2vb.CUF
		cd compiled; rm -f transpose_kernel_euler2vb.mod; ${fc} ${fflags} -c ../src/transpose_kernel_euler2vb.CUF; cd ..

compiled/transpose_kernel_vb2euler.mod: \
	src/transpose_kernel_vb2euler.CUF
		cd compiled; rm -f transpose_kernel_vb2euler.mod; ${fc} ${fflags} -c ../src/transpose_kernel_vb2euler.CUF; cd ..

compiled/transpose_kernel_orig2vb.mod: \
	src/transpose_kernel_orig2vb.CUF 
		cd compiled; rm -f transpose_kernel_orig2vb.mod; ${fc} ${fflags} -c ../src/transpose_kernel_orig2vb.CUF; cd ..

compiled/transpose_kernel_vb2orig.mod: \
	src/transpose_kernel_vb2orig.CUF 
		cd compiled; rm -f transpose_kernel_vb2orig.mod; ${fc} ${fflags} -c ../src/transpose_kernel_vb2orig.CUF; cd ..

compiled/transpose_kernel_lagrange.mod: \
	src/transpose_kernel_lagrange.CUF 
		cd compiled; rm -f transpose_kernel_lagrange.mod; ${fc} ${fflags} -c ../src/transpose_kernel_lagrange.CUF; cd ..

compiled/transpose_kernel_obc_river.mod: \
	src/transpose_kernel_obc_river.CUF 
		cd compiled; rm -f transpose_kernel_obc_river.mod; ${fc} ${fflags} -c ../src/transpose_kernel_obc_river.CUF; cd ..

##########################################
# compiling forcing-load subroutines ... #
##########################################
compiled/netcdf_check_mod.mod: \
	src/netcdf_check.cuf 
		cd compiled; rm -f netcdf_check_mod.mod; ${fc} ${fflags} -c ../src/netcdf_check.cuf; cd ..

compiled/load_forcing_mod.mod: \
    compiled/types_mod.mod \
	compiled/variables_mod.mod compiled/timers_mod.mod compiled/netcdf_check_mod.mod \
	compiled/transpose_kernel_orig2vb.mod compiled/transpose_kernel_vb2euler.mod \
	compiled/transpose_kernel_obc_river.mod src/load_forcing.CUF 
		cd compiled; rm -f load_forcing_mod.mod; ${fc} ${fflags} -c ../src/load_forcing.CUF; cd ..

############################################"
# compiling initialization subroutines ... #"
############################################"
compiled/init_grid_mod.mod: \
	compiled/variables_mod.mod compiled/netcdf_check_mod.mod src/init_grid.cuf
		cd compiled; rm -f init_grid_mod.mod; ${fc} ${fflags} -c ../src/init_grid.cuf; cd ..

compiled/init_advection_mod.mod: \
	compiled/variables_mod.mod compiled/netcdf_check_mod.mod compiled/advection_kernel.mod src/init_advection.CUF
		cd compiled; rm -f init_advection_mod.mod; ${fc} ${fflags} -c ../src/init_advection.CUF; cd ..

compiled/init_allocate_mod.mod: \
	compiled/variables_mod.mod compiled/bgc_dimensions_mod.mod compiled/bgc_dimensions_sedi_mod.mod src/init_allocate.cuf
		cd compiled; rm -f init_allocate_mod.mod; ${fc} ${fflags} -c ../src/init_allocate.cuf; cd ..

compiled/init_bgc_constants_mod.mod: \
	compiled/variables_mod.mod src/init_bgc_constants.cuf
		cd compiled; rm -f init_bgc_constants_mod.mod; ${fc} ${fflags} -c ../src/init_bgc_constants.cuf; cd ..

compiled/init_bgc_singlevariable_mod.mod: \
	compiled/variables_mod.mod compiled/transpose_kernel_orig2vb.mod src/init_bgc_singlevariable.CUF
		cd compiled; rm -f init_bgc_singlevariable_mod.mod; ${fc} ${fflags} -c ../src/init_bgc_singlevariable.CUF; cd ..

compiled/init_bgc_singlevariable_sedi_mod.mod: \
	compiled/sediment_utils.mod \
	compiled/variables_mod.mod compiled/bgc_dimensions_sedi_mod.mod compiled/transpose_kernel_orig2vb.mod src/init_bgc_singlevariable_sedi.CUF
		cd compiled; rm -f init_bgc_singlevariable_sedi_mod.mod; ${fc} ${fflags} -c ../src/init_bgc_singlevariable_sedi.CUF; cd ..

compiled/init_bgc_variables_mod.mod: \
	compiled/init_bgc_singlevariable_mod.mod src/init_bgc_variables.cuf 
		cd compiled; rm -f init_bgc_variables_mod.mod; ${fc} ${fflags} -c ../src/init_bgc_variables.cuf; cd ..

compiled/init_bgc_variables_sedi_mod.mod: \
	compiled/init_bgc_singlevariable_sedi_mod.mod src/init_bgc_variables_sedi.cuf 
		cd compiled; rm -f init_bgc_variables_sedi_mod.mod; ${fc} ${fflags} -c ../src/init_bgc_variables_sedi.cuf; cd ..

compiled/init_bgc_singleobc_mod.mod: \
	compiled/variables_mod.mod \
	compiled/init_forcing_mod.mod src/init_bgc_singleobc.CUF 
		cd compiled; rm -f init_bgc_singleobc_mod.mod; ${fc} ${fflags} -c ../src/init_bgc_singleobc.CUF; cd ..

compiled/init_bgc_obc_mod.mod: \
	compiled/init_bgc_singleobc_mod.mod src/init_bgc_obc.cuf 
		cd compiled; rm -f init_bgc_obc_mod.mod; ${fc} ${fflags} -c ../src/init_bgc_obc.cuf; cd ..

compiled/init_bgc_singleriver_mod.mod: \
	compiled/variables_mod.mod \
	compiled/init_forcing_mod.mod src/init_bgc_singleriver.CUF 
		cd compiled; rm -f init_bgc_singleriver_mod.mod; ${fc} ${fflags} -c ../src/init_bgc_singleriver.CUF; cd ..

compiled/init_bgc_river_mod.mod: \
	compiled/init_bgc_singleriver_mod.mod src/init_bgc_obc.cuf 
		cd compiled; rm -f init_bgc_river_mod.mod; ${fc} ${fflags} -c ../src/init_bgc_river.cuf; cd ..

compiled/init_transfer_mod.mod: \
	compiled/variables_mod.mod compiled/types_mod.mod src/init_transfer.cuf 
		cd compiled; rm -f init_transfer_mod.mod; ${fc} ${fflags} -c ../src/init_transfer.cuf; cd ..

compiled/init_forcing_mod.mod: \
	compiled/variables_mod.mod compiled/netcdf_check_mod.mod src/init_forcing.CUF \
	compiled/time_conversion_mod.mod compiled/load_forcing_mod.mod
		cd compiled; rm -f init_forcing_mod.mod; ${fc} ${fflags} -c ../src/init_forcing.CUF; cd ..

compiled/init_bgc_output_mod.mod: \
	compiled/types_mod.mod compiled/variables_mod.mod compiled/netcdf_check_mod.mod src/init_bgc_output.cuf
		cd compiled; rm -f init_bgc_output_mod.mod; ${fc} ${fflags} -c ../src/init_bgc_output.cuf; cd ..

compiled/init_bgc_output_sedi_mod.mod: \
	compiled/types_mod.mod compiled/variables_mod.mod compiled/netcdf_check_mod.mod src/init_bgc_output_sedi.cuf
		cd compiled; rm -f init_bgc_output_sedi_mod.mod; ${fc} ${fflags} -c ../src/init_bgc_output_sedi.cuf; cd ..

compiled/init_output_mod.mod: \
	compiled/types_mod.mod compiled/variables_mod.mod compiled/init_bgc_output_mod.mod \
	compiled/netcdf_check_mod.mod src/init_output.cuf 
		cd compiled; rm -f init_output_mod.mod; ${fc} ${fflags} -c ../src/init_output.cuf; cd ..

compiled/restart_mod.mod: \
	compiled/variables_mod.mod compiled/timers_mod.mod \
	compiled/advection_mod.mod \
	compiled/transpose_kernel_vb2euler.mod compiled/transpose_kernel_euler2vb.mod \
	compiled/time_conversion_mod.mod \
	compiled/netcdf_check_mod.mod compiled/cuda_check_mod.mod \
	src/restart.CUF 
		cd compiled; rm -f restart_mod.mod; ${fc} ${fflags} -c ../src/restart.CUF; cd ..

compiled/main_init_mod.mod: \
	compiled/variables_mod.mod compiled/timers_mod.mod \
	compiled/sediment_utils.mod \
	compiled/time_conversion_mod.mod \
	compiled/init_grid_mod.mod compiled/init_advection_mod.mod \
	compiled/init_allocate_mod.mod \
	compiled/init_bgc_constants_mod.mod compiled/init_bgc_variables_mod.mod \
	compiled/init_bgc_obc_mod.mod compiled/init_bgc_river_mod.mod \
    compiled/init_bgc_variables_sedi_mod.mod \
	compiled/init_transfer_mod.mod compiled/init_forcing_mod.mod \
	compiled/init_output_mod.mod compiled/restart_mod.mod \
	src/main_init.cuf 
		cd compiled; rm -f main_init_mod.mod; ${fc} ${fflags} -c ../src/main_init.cuf; cd ..

#######################################"
# compiling time step subroutines ... #"
#######################################"

compiled/bgc_kernel_sedi.mod: \
    compiled/cuda_settings.inc \
	compiled/variables_mod.mod compiled/timers_mod.mod \
	src/bgc_kernel_sedi.CUF
		cd compiled; rm -f bgc_kernel_sedi.mod; ${fc} ${fflagsbgc} -c ../src/bgc_kernel_sedi.CUF; cd ..
	
compiled/main_timestep_mod.mod: \
    compiled/cuda_settings.inc \
	compiled/cuda_check_mod.mod \
	compiled/variables_mod.mod compiled/timers_mod.mod \
	compiled/time_conversion_mod.mod \
	compiled/light_calculation_mod.mod \
	compiled/river_mod.mod \
	compiled/advection_mod.mod \
	compiled/vertdiff_kernel.mod \
	compiled/load_forcing_mod.mod \
	compiled/bgc_kernel_wat.mod compiled/bgc_kernel_sur.mod compiled/bgc_kernel_sed.mod compiled/bgc_kernel_sursed.mod \
	compiled/bgc_kernel_sedi.mod \
	compiled/bgc_dimensions_mod.mod \
        compiled/bgc_kernel_sedi.mod \
	compiled/transpose_kernel_vb2euler.mod compiled/transpose_kernel_euler2vb.mod \
	compiled/transpose_kernel_vb2orig.mod \
	compiled/bgc_output_mod.mod \
	compiled/sediment_utils.mod \
	compiled/init_bgc_output_sedi_mod.mod \
	src/main_timestep.CUF
		cd compiled; rm -f main_timestep_mod.mod; ${fc} ${fflags} -c ../src/main_timestep.CUF; cd ..

compiled/bgc_output_mod.mod: \
    compiled/variables_mod.mod compiled/timers_mod.mod \
	compiled/netcdf_check_mod.mod \
	src/bgc_output.CUF
	    cd compiled; rm -f bgc_output_mod.mod; ${fc} ${fflags} -c ../src/bgc_output.CUF; cd ..

compiled/light_calculation_mod.mod: \
    compiled/cuda_settings.inc \
	compiled/light_kernel.mod \
	compiled/cuda_check_mod.mod \
    compiled/variables_mod.mod \
	src/light_calculation.CUF
	    cd compiled; rm -f light_calculation_mod.mod; ${fc} ${fflags} -c ../src/light_calculation.CUF; cd ..

compiled/advection_mod.mod: \
    compiled/cuda_settings.inc \
	compiled/transpose_kernel_lagrange.mod \
	compiled/advection_kernel.mod \
	compiled/cuda_check_mod.mod \
    compiled/variables_mod.mod \
	compiled/timers_mod.mod \
	src/advection.CUF
	    cd compiled; rm -f advection_mod.mod; ${fc} ${fflags} -c ../src/advection.CUF; cd ..

compiled/river_mod.mod: \
    compiled/cuda_settings.inc \
	compiled/cuda_check_mod.mod \
    compiled/variables_mod.mod \
	compiled/timers_mod.mod \
	src/river.CUF
	    cd compiled; rm -f river_mod.mod; ${fc} ${fflagsbgc} -c ../src/river.CUF; cd ..

##########################################"
# compiling finalization subroutines ... #"
##########################################"

compiled/main_final_mod.mod: \
	compiled/variables_mod.mod compiled/timers_mod.mod \
	compiled/netcdf_check_mod.mod compiled/restart_mod.mod \
	src/main_final.cuf
		cd compiled; rm -f main_final_mod.mod; ${fc} ${fflags} -c ../src/main_final.cuf; cd ..

##############################"
# compiling main program ... #"
##############################"
compiled/main.o: \
	compiled/variables_mod.mod compiled/timers_mod.mod \
	compiled/main_init_mod.mod compiled/main_timestep_mod.mod \
	compiled/main_final_mod.mod src/main.cuf
		cd compiled; rm -f main.o; ${fc} ${fflags} -c ../src/main.cuf; cd ..
