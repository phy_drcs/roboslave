module bgc_kernel_WAT
! solve biogeochemical tracer equations for interior water cells

#include "cuda_settings.inc"

contains

  attributes(device) real function theta(x)
    real :: x
    theta = max(sign(1.0,x),0.0)
  end 

  attributes(device) real function power(base,exponent)
    real :: base,exponent
    power = base**exponent
  end

  attributes(device) subroutine check_for_getting_negative(old_value, change, num_tracer, timestep_fraction, which_tracer_exhausted)
    real,    intent(in)    :: old_value, change
    integer, intent(in)    :: num_tracer
    real,    intent(inout) :: timestep_fraction
    integer, intent(inout) :: which_tracer_exhausted

    real :: timestep_fraction_new

    ! check if tracer  was exhausted from the beginning and is still consumed
    if ((old_value .le. 0.0) .and. (change  .lt. 0.0)) then
      timestep_fraction = 0.0
      which_tracer_exhausted = num_tracer
    end if
    ! check if tracer <name> was present, but got exhausted
    if ((old_value .gt. 0.0) .and. (old_value + change .lt. 0.0)) then
      timestep_fraction_new = old_value / (0.0 - change)
      if (timestep_fraction_new .le. timestep_fraction) then
        which_tracer_exhausted = num_tracer
        timestep_fraction = timestep_fraction_new
      end if
    end if
  end subroutine check_for_getting_negative

  attributes(global) launch_bounds(CUDA_TPB_WAT, CUDA_BPM_WAT)&
                     subroutine bgc_timestep_WAT(varEuler, lightEuler, parEuler, constants, dt, outEuler, &
                                                 outOpacity, iMax, mMax, nvarMax, nparMax, nconMax, nVarFields)
  ! This subroutine will do the biological time step
  ! The first element in launch_bounds states how many threads may execute in one block
  !   => reduce this number if the kernel gets large and fails to run
  ! The second element states how many blocks are needed at the same time to start
  !   => increase this number accordingly to get a good speed, their product should be at least 512
    implicit none
    real, intent(inout) :: varEuler(:,:)                 ! variables on Eulerian grid (mol/kg) 
                                                         ! as well as their vertical velocity (m/s) and possibly diffusivity (m2/s)
                                                         ! dimension(miMax,nvarMax*nVarFields)
    real, intent(in)    :: lightEuler(:)                 ! average photosynthetically active radiation in the cell (W/m2)
                                                         ! dimension(miMax)
    real, intent(in)    :: parEuler(:,:)                 ! physical parameters on Eulerian grid (different units)
                                                         ! dimension(miMax,nparMax)
    real, intent(in)    :: constants(:,:)                ! bgc model constants (different units)
                                                         ! dimension(mMax,nconMax)
    real, value         :: dt                            ! time step (days)
    real, intent(inout) :: outEuler(:,:)                 ! output quantities on Eulerian grid (different units)
                                                         ! dimension(miMax,noutMax)
    real, intent(inout) :: outOpacity(:)                 ! opacity of water (1/m)
                                                         ! dimension(miMax)
    integer, value      :: iMax                          ! number of grid points
    integer, value      :: mMax                          ! number of ensemble members
    integer, value      :: nvarMax                       ! number of variables
    integer, value      :: nparMax                       ! number of physical parameters
    integer, value      :: nconMax                       ! number of BGC constants
    integer, value      :: nVarFields                    ! number of fields to save for each state variable

    integer :: cgt_iteration               ! current number of iteration in the iterative loop
    integer :: m                           ! ensemble member index
    integer :: mi, miMax                   ! ensemble member and spatial location combined index
    integer :: n                           ! index in loop over variables, constants, parameters etc.
    integer :: number_of_loop              ! loop counter for the positive Euler-forward (pEf) scheme
    integer :: which_tracer_exhausted      ! stores index of the first tracer that drops below zero, -1 if all stay positive
    real    :: timestep_fraction           ! which fraction of the timestep can we do in this step of the pEf scheme
    real    :: timestep_fraction_new       
    real    :: fraction_of_total_timestep  ! how much of the original time step is remaining in the pEf scheme

    ! physical parameters - local
    real :: cgt_longitude           ! longitude of grid cell           [deg]
    real :: cgt_latitude            ! latitude of grid cell            [deg]
    real :: cgt_cellarea            ! horizontal area of grid cell     [m2]
    real :: cgt_bottomdepth         ! distance between sea floor and bottom of current cell [m]
    real :: cgt_cellheight          ! cell height                      [m]
    real :: cgt_seafloordepth       ! depth of the sea floor           [m]
    real :: cgt_temp                ! potential temperature            [Celsius]
    real :: cgt_sali                ! salinity                         [g/kg]
    real :: cgt_density             ! density                          [kg/m3]
    real :: cgt_diffusivity         ! diffusivity at cell bottom for T [m**2/s]

    ! physical parameters - global
    real            :: cgt_timestep            ! timestep in days      [days]
    integer         :: cgt_year, cgt_dayofyear, cgt_mymonth, cgt_myday, cgt_myhour, cgt_myminute, cgt_mysecond ! some date/time variables
    real            :: cgt_hour                ! fractional hour (0..23.9999)
    real, parameter :: cgt_in_sediment=0.0     ! whether we are below the sediment surface

    ! biology-dependent forcing
    real :: cgt_light               ! downward light flux (PAR)    [W/m2]

    ! opacity for output
    real :: cgt_opacity

    ! constants
<constants>
    real :: <name> ! <description>
</constants>    

    ! tracers
<tracers vertLoc=WAT>
    real :: <name> ! <description>
    real :: change_of_<name>
</tracers>
    ! keep old values of possibly negative variables in the cache 
    ! to avoid loading them again during Euler-forward tracer update
<tracers vertLoc=WAT>
    real :: cgt_old_value_of_<name>
</tracers>

    ! auxiliary variables
    real :: temp1, temp2, temp3, temp4, temp5, temp6, temp7, temp8, temp9
<auxiliaries vertLoc=WAT>
    real :: <name> ! <description>
</auxiliaries>

    ! process rates
<processes vertLoc=WAT>
    real :: <name> ! <description>
    real :: total_rate_<name>
</processes>

    ! process limitations
<tracers vertLoc=WAT>
  <limitations>
    real :: <name>
  </limitations>
</tracers>

    miMax = size(lightEuler)
    mi    = blockDim%x * (blockIdx%x - 1) + threadIdx%x
    m     = MOD(mi-1,mMax)+1
    if (mi .le. miMax) then
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! STEP 1: LOAD LOCAL VALUES !
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! load constants !
      !!!!!!!!!!!!!!!!!!
      n=1
<constants>
      <name> = constants(m,n)
      n=n+1
</constants>   

      !!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! load physical parameters ! 
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!
      cgt_timestep      = dt
      cgt_longitude     = parEuler(mi,1)
      cgt_latitude      = parEuler(mi,2)
      cgt_cellarea      = parEuler(mi,3)
      cgt_bottomdepth   = parEuler(mi,4)
      cgt_cellheight    = parEuler(mi,5)
      cgt_seafloordepth = parEuler(mi,6)
      cgt_temp          = parEuler(mi,7) 
      cgt_sali          = parEuler(mi,8)
      cgt_density       = parEuler(mi,9)
      cgt_diffusivity   = parEuler(mi,10)

      !!!!!!!!!!!!!!
      ! load light !
      !!!!!!!!!!!!!!
      cgt_light = lightEuler(mi)

      !!!!!!!!!!!!!!!!!!
      ! load variables !
      !!!!!!!!!!!!!!!!!!
<tracers vertLoc=WAT>
      <name> = varEuler(mi,<numTracer vertLoc=WAT>)
      cgt_old_value_of_<name> = <name>  ! store the old value, in case it is negative, since it may be clipped to zero later
</tracers>
<tracers vertLoc=WAT; isPositive=1>
      <name> = max(<name>,0.0)
</tracers>

      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! STEP 2: CALCULATE COLORED ELEMENT CONCENTRATIONS !
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
<celements isTracer=1>
      <total> = &
      <containingTracers vertLoc=WAT>
        max(0.0,<ct>)*<ctAmount> + &
      </containingTracers>
        0.0
</celements>

      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! STEP 3: CALCULATE AUXILIARY VARIABLES !
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! iterative loop !
      !!!!!!!!!!!!!!!!!!
<auxiliaries vertLoc=WAT; calcAfterProcesses=0; isZGradient=0; iterations/=0>
      <name> = <iterInit>
</auxiliaries>

      do cgt_iteration=1,<maxIterations>
<auxiliaries vertLoc=WAT; calcAfterProcesses=0; isZGradient=0; iterations/=0>
        if (cgt_iteration .le. <iterations>) then
          ! <description> :
          temp1 = <temp1>
          temp2 = <temp2>
          temp3 = <temp3>
          temp4 = <temp4>
          temp5 = <temp5>
          temp6 = <temp6>
          temp7 = <temp7>
          temp8 = <temp8>
          temp9 = <temp9>
          <name> = <formula>
        endif
</auxiliaries>
      enddo

      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! normal (non-iterative) auxiliary variables !
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
<auxiliaries vertLoc=WAT; calcAfterProcesses=0; isZGradient=0; iterations=0>
      ! <description> :
      temp1 = <temp1>
      temp2 = <temp2>
      temp3 = <temp3>
      temp4 = <temp4>
      temp5 = <temp5>
      temp6 = <temp6>
      temp7 = <temp7>
      temp8 = <temp8>
      temp9 = <temp9>
      <name> = <formula>

</auxiliaries>


      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! STEP 4: CALCULATE INITIAL PROCESS LIMITATIONS !
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
<tracers vertLoc=WAT>
  <limitations>
      <name> = <formula>
  </limitations>
</tracers>

      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! STEP 5: POSITIVE EULER-FORWARD TIME INTEGRATION LOOP !
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! initializations !
      !!!!!!!!!!!!!!!!!!!
<processes vertLoc=WAT>
        total_rate_<name> = 0.0
</processes>

      !!!!!!!!!!!!!!!!!!!!!!!!
      ! try a full time step !
      !!!!!!!!!!!!!!!!!!!!!!!!
      number_of_loop             = 1
      fraction_of_total_timestep = 1.0

      do while (cgt_timestep .gt. 0.0)
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        ! STEP 5.1: calculate full process rates !
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
<processes vertLoc=WAT>
        ! <description> :
        <name> = <turnover>
        <name> = max(<name>,0.0)

</processes>
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        ! STEP 5.2: calculate possible euler-forward change (in a full timestep) !
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
<tracers vertLoc=WAT>
        change_of_<name> = 0.0
</tracers>
<tracers vertLoc=WAT; hasTimeTendenciesVertLoc=WAT>
        change_of_<name> = change_of_<name> + cgt_timestep*(0.0 &
        <timeTendencies vertLoc=WAT>
          <timeTendency> & ! <description>
        </timeTendencies>
        )
</tracers>

        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        ! STEP 5.3: calculate maximum fraction of the timestep before some tracer gets exhausted !
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        timestep_fraction = 1.0
        which_tracer_exhausted = -1

<tracers vertLoc=WAT; isPositive=1>
        call check_for_getting_negative(cgt_old_value_of_<name>, change_of_<name>, <numTracer>, &
                                        timestep_fraction, which_tracer_exhausted)
</tracers>        

        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        ! STEP 5.4: update the limitations: processes limited by this tracer become zero in the future !
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
<tracers isPositive=1; vertLoc=WAT>
        if (<numTracer> .eq. which_tracer_exhausted) then
        <limitations>
          <name> = 0.0
        </limitations>
        endif
</tracers>

        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        ! STEP 5.5: apply a Euler-forward timestep with the fraction of the time !
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
<tracers vertLoc=WAT>
        ! tracer <name> (<description>):
        cgt_old_value_of_<name> = cgt_old_value_of_<name> + change_of_<name> * timestep_fraction
</tracers>

        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        ! STEP 5.6: save process rates to obtain total rate over full timestep in the end !
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
<processes vertLoc=WAT>
        total_rate_<name> = total_rate_<name> + <name> * timestep_fraction * fraction_of_total_timestep
</processes>

        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        ! STEP 5.7: set timestep to remaining timestep only !
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        cgt_timestep = cgt_timestep * (1.0 - timestep_fraction)                             ! remaining timestep
        fraction_of_total_timestep = fraction_of_total_timestep * (1.0 - timestep_fraction) ! how much of the original timestep is remaining

         !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         ! STEP 5.8: error check in case that pEf scheme does not converge !
         !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         if (number_of_loop .gt. 100) then
           write(*,*) "ERROR: pEf scheme does not converge in bgc_kernel_WAT"
           stop
         end if

        number_of_loop=number_of_loop+1
      end do  ! end of positive Euler-forward scheme

      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! STEP 6: APPLY CHANGES CALCULATED IN THE pEf LOOP !
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! update the tracer array
<tracers vertLoc=WAT; hasTimeTendenciesVertLoc=WAT>
      varEuler(mi,<numTracer vertLoc=WAT>) = cgt_old_value_of_<name>
</tracers>
      ! get tracer values again for calculating late auxiliary variables
<tracers vertLoc=WAT; isPositive=0>
      <name> = cgt_old_value_of_<name>
</tracers>
<tracers vertLoc=WAT; isPositive=1>
      <name> = max(cgt_old_value_of_<name>,0.0)
</tracers>
      ! get process rates for calculating late auxiliary variables
<processes vertLoc=WAT>
      <name> = total_rate_<name>
</processes>

      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! STEP 7: CALCULATE LATE AUXILIARY VARIABLES !
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
<auxiliaries vertLoc=WAT; calcAfterProcesses=1; isZGradient=0>
      ! <description> :
      temp1 = <temp1>
      temp2 = <temp2>
      temp3 = <temp3>
      temp4 = <temp4>
      temp5 = <temp5>
      temp6 = <temp6>
      temp7 = <temp7>
      temp8 = <temp8>
      temp9 = <temp9>
      <name> = <formula>

</auxiliaries>
      !!!!!!!!!!!!!!!!!!!!!!!!!!
      ! STEP 6: WRITING OUTPUT !
      !!!!!!!!!!!!!!!!!!!!!!!!!!
      ! Output is added to existing values in the output array. 
      ! This is because the output frequency can be several time steps long.
      ! Then we need to divide by the number of time steps per output time step in the end.
      n = 1
<tracers vertLoc=WAT; isOutput=1>
      outEuler(mi,n) = outEuler(mi,n) + <name>
      n=n+1
</tracers>
<auxiliaries vertLoc=WAT; isOutput=1>
      outEuler(mi,n) = outEuler(mi,n) + <name>
      n=n+1
</auxiliaries>
<processes vertLoc=WAT; isOutput=1>
      outEuler(mi,n) = outEuler(mi,n) + <name>
      n=n+1
</processes>

! write vertical velocities
<tracers vertLoc=WAT>
      varEuler(mi,nvarMax + <numTracer vertLoc=WAT>) = <vertSpeed>/(24.0*3600.0) ! vertical speed of <name> (m/s)
</tracers>
      if (nVarFields .ge. 3) then
<tracers vertLoc=WAT>
        varEuler(mi,2*nvarMax + <numTracer vertLoc=WAT>) = <vertDiff> ! additional vertical diffusivity of <name> (m2/s)
</tracers>
      end if

      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! STEP 7: SAVING OPACITY OF WATER INGREDIENTS !
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      cgt_opacity = &
<tracers vertLoc=WAT; opacity/=0>
        <name> * <opacity> * cgt_density + &
</tracers>
        0.0

<auxiliaries vertLoc=WAT; name=clear_water_opacity>
      ! an auxiliary variable "clear_water_opacity" exists, so add this to the tracer-based opacity
      cgt_opacity = cgt_opacity + clear_water_opacity
</auxiliaries>
<auxiliaries vertLoc=WAT; name=gamma0>
      ! an auxiliary variable "gamma0" exists, so add this to the tracer-based opacity
      cgt_opacity = cgt_opacity + gamma0
</auxiliaries>
<constants name=clear_water_opacity>
      ! a constant "clear_water_opacity" exists, so add this to the tracer-based opacity
      cgt_opacity = cgt_opacity + clear_water_opacity
</constants>
<constants name=gamma0>
      ! a constant "gamma0" exists, so add this to the tracer-based opacity
      cgt_opacity = cgt_opacity + gamma0
</constants>


<auxiliaries vertLoc=WAT; name=opacity>
      ! an auxiliary variable "opacity" exists, so override the tracer-based calculation
      cgt_opacity = opacity
</auxiliaries>

      outOpacity(mi) = cgt_opacity
      
    end if
  end subroutine bgc_timestep_WAT 
end module bgc_kernel_WAT

